  # !/bin/bash

# Based on: https://docs.gitlab.com/ee/ci/ssh_keys/README.html

# Install ssh-agent if not already installed, it is required by Docker.
# (change apt-get to yum if you use an RPM-based image)
# - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
# Run ssh-agent (inside the build environment)
eval $(ssh-agent -s)

# Add the SSH key stored in SSH_PRIVATE_KEY variable to the agent store
# We're using tr to fix line endings which makes ed25519 keys work
# without extra base64 encoding.
# https://gitlab.com/gitlab-examples/ssh-private-key/issues/1#note_48526556
echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -

# Create the SSH directory and give it the right permissions
mkdir -p ~/.ssh
chmod 700 ~/.ssh

# Use ssh-keyscan to scan the keys of your private server.
ssh-keyscan gitlab.mpcdf.mpg.de >> ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts

# Set the user name and email.
git config --global user.name $GITLAB_USER_NAME
git config --global user.email $GITLAB_USER_EMAIL


# Clone the private repository
git clone git@gitlab.mpcdf.mpg.de:nomad-lab/analytics-deployment.git /tmp/analytics-deployment

cd /tmp/analytics-deployment

# Update the tag of the docker image

sed  -i "s/^    tag\:.*/    tag\: production$CI_COMMIT_SHORT_SHA/g" deployments/hub/config.yaml
sed  -i "s/^    tag\:.*/    tag\: production$CI_COMMIT_SHORT_SHA/g" deployments/hub/public.yaml

# Finally, commit and push the changes
git add deployments/hub/config.yaml
git add deployments/hub/public.yaml
git commit -m "CI: Update the hub image for production ($CI_PIPELINE_URL)"
git push

rm -rf /tmp/analytics-deployment
