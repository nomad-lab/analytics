from PIL import Image, ImageDraw, ImageFont
import textwrap as tr
import json
import os

if __name__ == '__main__':

	dir_list = os.listdir()
	for directory in dir_list:
		try:
			with open(directory + '/metainfo.json', 'r') as f:
			    file=f.read()
			metadata = json.loads(file)
			title = metadata['title']
			authors = metadata['authors']
			update = metadata['updated']

			title_lines = tr.wrap(title, width=35)
			title='\n'.join(title_lines)
			update = 'Last update: ' + update
			authors_list = ''	
			for author in authors: 
			    first_name = author.replace(',',' ').split("  ")[1] 
			    last_name = author.replace(',',' ').split("  ")[0]
			    authors_list = authors_list + first_name + ' ' + last_name
			    if authors.index(author) < len(authors) -2 :
			        authors_list = authors_list + ', '
			    else:
			        if authors.index(author) == len(authors) -2 :
			            authors_list = authors_list + ' and '
			authors_lines = tr.wrap(authors_list, width=60)
			authors = '\n'.join(authors_lines)

			header = Image.open('files/AIT_bg_title.jpg')

			draw = ImageDraw.Draw(header)
			font_title = ImageFont.truetype ('files/Titillium/TitilliumWeb-Bold.ttf', 100)
			font_authors = ImageFont.truetype ('files/Titillium/TitilliumWeb-Italic.ttf', 60)
			font_update = ImageFont.truetype ('files/Titillium/TitilliumWeb-Regular.ttf', 50)
			draw.multiline_text((250,100), title, fill='white', font=font_title)
			draw.multiline_text((250,600), authors, fill='white', font=font_authors)
			draw.multiline_text((250,950), update, fill='white', font=font_update)

			list_words_dir = directory.replace('-',' ').split()[1:]
			tutorial_name = list_words_dir[0]
			for word in list_words_dir[1:]:
				tutorial_name = tutorial_name + '_' + word

			save_path = '/opt/tutorials/' + directory + '/assets/' + tutorial_name  + '/header.jpg'
			print(save_path)
			header.save(save_path)
			print('header saved')
		except:
			pass
