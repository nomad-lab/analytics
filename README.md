# Single-user Jupyter notebook image for analytics

This notebook folder contains Dockerfile for a single user notebook image which is based on Jupyter docker stack.
The Docker image is suitable for running/testing locally and for deploying it by the JupyterHub on a Kubernetes cluster.


## Building/using your own Docker image

### Cloning the repository and checkout the develop branch

```bash
git clone --recursive https://gitlab.mpcdf.mpg.de/nomad-lab/analytics.git
cd analytics
git checkout develop
git submodule update --init
```

### Building the docker image 
```bash
docker build -t analytics:develop .
```
### Deploy the Docker image locally
```bash
docker run --rm -p 8888:8888 analytics:develop
```

### Create new tutorial
To create a new tutorial, you can use the template [./analytics-tutorial-template](https://gitlab.mpcdf.mpg.de/nomad-lab/analytics/-/tree/develop/analytics-tutorial-template). Substitute 'tutorial template' with a short name that identifies the tutorial. 

Please observe the following: 
- the underscore/hyphen convention used - in all files, also within the directory.
- employ lowercase letters for file names.
- clear the cells output before saving.
- update the 'setup.py' file with all Python packages that need to be installed with pip.
- update the 'metainfo.json' file with all metadata.  

### Test new tutorial
If you move the new notebook directory into the 'tutorials' folder, and then build the image, you can test the notebook in the NOMAD AI Toolkit environment.
To install in the NOMAD AI Toolkit environment functions that are defined in analytics-tutorial-template, add the command `RUN pip install analytics-tutorial-template` in Dockerfile. Then: 

```bash
mv analytics-tutorial-template tutorials
docker build -t analytics:develop .
docker run --rm -p 8888:8888 analytics:develop
```
The 'tutorial_template.ipynb' notebook will then appear in the tutorials list.

### Getting published
If you wish to publish your notebook in the NOMAD AI Toolkit, please contact us (ghiringhelli@fhi-berlin.mpg.de).
