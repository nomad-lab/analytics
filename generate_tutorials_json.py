#!/usr/bin/env python

import json
import sys
from pathlib import Path

cwd = Path(__file__).parent

def iter_folders(path):
    for file in path.glob('**/metainfo.json'):
        with open(file) as f:
            yield json.load(f)


if __name__ == '__main__':
    # Defining the relative path of the tutorials
    outputfile = cwd / 'tutorials.json'
    path = cwd / 'tutorials/'

    list_of_tutorials = {'tutorials': list(iter_folders(path))}

    print(json.dumps(list_of_tutorials, indent=2))

    with open(outputfile , 'w') as f:

            json.dump(list_of_tutorials, f, indent=2)