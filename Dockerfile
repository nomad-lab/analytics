FROM jupyter/tensorflow-notebook:2ce7c06a61a1
LABEL maintainer="Luca Ghiringhelli <ghiringhelli@fhi-berlin.mpg.de>"

# ================================================================================
# Linux applications and libraries
# ================================================================================


USER root
RUN apt-get update \
 && apt-get install -y -q --no-install-recommends \
        gcc \
        gfortran \
        liblapack-dev \
        libblas-dev \
        libnetcdf-dev \
        netcdf-bin \
        libxpm-dev \
        libgsl-dev \
        lsof \
        vim \
        git-lfs\
        openjdk-8-jdk \
        xvfb \
        cmake \
        openssh-server \
        openssh-client \
        graphviz \
        dvipng \
        libboost-system-dev libboost-filesystem-dev libboost-mpi-dev libboost-serialization-dev \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

WORKDIR /opt/OpenMPI/
RUN wget https://download.open-mpi.org/release/open-mpi/v4.1/openmpi-4.1.4.tar.bz2 &&\
    tar xjf openmpi-4.1.4.tar.bz2 &&\
    cd openmpi-4.1.4/ &&\
    mkdir build &&\
    cd build &&\
    ../configure --prefix=/opt/OpenMPI/ &&\
    make install &&\
    cd ../../

ENV PATH /opt/OpenMPI/bin/:$PATH
ENV LD_LIBRARY_PATH /opt/OpenMPI/lib/:$LD_LIBRARY_PATH

USER $NB_UID
ENV PATH /opt/OpenMPI/bin/:$PATH
ENV LD_LIBRARY_PATH /opt/OpenMPI/lib/:$LD_LIBRARY_PATH

# Dependecies:
# - quippy: gcc gfortran liblapack-dev libblas-dev libnetcdf-dev netcdf-bin libxpm-dev libgsl-dev

# ================================================================================
# Python environment
# ================================================================================

# This part contains the installation of all the common/basic python packages.
# All the other package will be installed by the tutorial's own setup scripts.
# Add any additional packages you want available for use in a Python 3 notebook
# to the first line here (e.g., nglview, jupyter_contrib_nbextensions, etc.)
# https://github.com/ipython-contrib/jupyter_contrib_nbextensions

RUN conda install conda=4.11
RUN pip install --upgrade pip
RUN pip install mpi4py
RUN conda install --quiet --yes \
    'pytorch::pytorch-cpu==1.1.0' \
    'pytorch::torchvision-cpu==0.3.0' \
    'nglview==2.7.0' \
    'jupyter_contrib_nbextensions==0.5.1' \
    'jupyter_nbextensions_configurator==0.4.1' \
    'ase' \
    'asap3' \
    'dscribe' \
    'pytest' \
    'orjson' \
    'hdbscan' \
    'pyyaml==6' \
    'numba==0.52' \
    'scikit-learn==0.24' \
    'pandas==1.2.5'\
    'urllib3==1.26.5'\
    'nest-asyncio '\
    'seaborn' \
 && conda install -c plotly plotly-orca \
 && conda install -c conda-forge umap-learn \
 && conda clean -tipsy \
 && jupyter nbextensions_configurator enable --user \
 && jupyter nbextension install nglview --py --sys-prefix \
 && jupyter nbextension enable nglview --py --sys-prefix \
 && jupyter nbextension enable execute_time/ExecuteTime \
 && jupyter nbextension enable init_cell/main \
 && jupyter nbextension enable collapsible_headings/main \
 && fix-permissions $CONDA_DIR \
 && fix-permissions /home/$NB_USER

# Dependecies:
# - ase: many
# - nglview: many
# - asap3: grain-boundaries

# fixing pip installation issue of nomad-lab dependency: orjson

# ================================================================================
#  QMMLPACK
# ================================================================================

# for the test TMPDIR has to be defined
ENV TMPDIR "/tmp/"

WORKDIR /opt/qmmlpack
COPY 3rdparty/qmmlpack .

USER root

RUN ./make --debug --verbose cpp --include-path /usr/include/gsl/ --library-path /usr/lib/
RUN ./make --debug --verbose python --include-path /usr/include/gsl/ --library-path /usr/lib/

RUN chown -R $NB_USER:$NB_GID $PWD
USER $NB_USER


#RUN make -v install
ENV PYTHONPATH "/opt/qmmlpack/python":$PYTHONPATH

# The CML_PLUGINS environment variable tells cmlkit to register the cscribe plugin, which provides the dscribe interface.
ENV CML_PLUGINS "cscribe"
ENV CML_DATASET_PATH $HOME/tutorials/data/cmlkit/

# Dependecies:
# - ase: many
# - nglview: many
# - asap3: grain-boundaries

# ================================================================================
#  QUIP + GAP + quippy
# ================================================================================

# All the QUIPs go here; added to path in the end.
WORKDIR /opt/quip

# QUIP for general use is the OpenMP version.
ENV QUIP_ARCH linux_x86_64_gfortran_openmp
ENV QUIP_INSTALLDIR /opt/quip/bin

COPY 3rdparty/quip .
COPY 3rdparty/gap src/GAP
COPY files/Makefile.inc build/$QUIP_ARCH/
# COPY files/GIT_VERSION .
# COPY files/GAP_VERSION src/GAP/

USER root
RUN chown -R $NB_USER:$NB_GID $PWD
USER $NB_USER

# Installs with no suffix, e.g. quip
RUN make \
 && make install

# Installs quippy
RUN pip install src/f90wrap \
 && make install-quippy

# Make quippy executable available from terminal
ENV PATH /opt/quip/bin:$PATH

# ================================================================================
#  SISSO++
# ================================================================================

WORKDIR /opt/cpp_sisso

COPY 3rdparty/cpp_sisso .
WORKDIR /opt/cpp_sisso/build
USER root
RUN cmake -C ../cmake/toolchains/gnu_param_py.cmake -DEXTERNAL_BOOST=ON ../ \
&& make install

# ================================================================================
#  KERAS-VIS
# ================================================================================

WORKDIR /opt/keras-vis

COPY 3rdparty/keras-vis .
RUN pip install .

# ================================================================================
#  ATOMIC FEATURES
# ================================================================================

RUN pip install nomad-lab --extra-index-url https://gitlab.mpcdf.mpg.de/api/v4/projects/2187/packages/pypi/simple

WORKDIR /opt/atomic_features

COPY 3rdparty/atomic-features-package ./atomic-features-package
USER root
RUN pip install ./atomic-features-package

# ================================================================================
#  CMLKIT
# ================================================================================

WORKDIR /opt/cmlkit

COPY 3rdparty/cmlkit ./cmlkit
USER root
RUN pip install ./cmlkit

# ================================================================================
# Install all of the package dependencies of the tutorials
# ================================================================================

RUN conda install pynndescent

WORKDIR /opt/tutorials
COPY tutorials/ .

RUN pip install ./analytics-arise \
&& pip install 'git+https://github.com/AndreasLeitherer/ARISE.git'
RUN pip install ./analytics-atomic-features
RUN pip install ./analytics-clustering-tutorial
RUN pip install ./analytics-cmlkit
RUN pip install ./analytics-co2-sgd-tutorial
RUN pip install ./analytics-compressed-sensing
RUN pip install ./analytics-convolutional-nn
RUN pip install ./analytics-decision-tree
RUN pip install ./analytics-descriptor-role
RUN pip install ./analytics-domain-of-applicability
RUN pip install ./analytics-dos-similarity-search
RUN pip install ./analytics-error-estimates
RUN pip install ./analytics-exploratory-analysis
RUN pip install ./analytics-gap-si-surface
RUN pip install ./analytics-grain-boundaries
RUN pip install ./analytics-hierarchical-sisso
RUN pip install ./analytics-kaggle-competition
RUN pip install ./analytics-krr4mat
RUN pip install ./analytics-nn-regression
RUN pip install ./analytics-perovskites-tolerance-factor
RUN pip install ./analytics-query-nomad-archive
RUN pip install ./analytics-sgd-alloys-oxygen-reduction-evolution
RUN pip install ./analytics-sgd-propylene-oxidation-hte
RUN pip install ./analytics-soap-atomic-charges
RUN pip install ./analytics-tcmi
RUN pip install ./analytics-tetradymite-PRM2020

USER root

# Trick to stop caching
ARG CACHE_DATE=2021-1-26

# Create headers for all tutorials
COPY generate_headers.py .
COPY files files
RUN pip install Pillow
RUN python generate_headers.py

# Customize jupyter
WORKDIR $HOME
COPY files/custom.css custom.css
COPY files/logo.png logo.png
COPY files/Titillium Titillium
RUN mkdir .jupyter/custom
RUN mv custom.css .jupyter/custom \
 && mv logo.png .jupyter/custom \
 && mv Titillium .jupyter/custom
ARG TUTORIALS_HOME=$HOME/tutorials


RUN fix-permissions /opt/tutorials \
 && fix-permissions $CONDA_DIR

# Copy all the notebooks of the tutorials
WORKDIR $TUTORIALS_HOME
RUN cp /opt/tutorials/*/*.ipynb . \
 && jupyter-trust -y *.ipynb

# Copy images or other assets may required by the tutorials
WORKDIR $TUTORIALS_HOME/assets
RUN cp -r /opt/tutorials/*/assets/*  .

# Copy data may be required by the tutorials
WORKDIR $TUTORIALS_HOME/data
RUN cp -r /opt/tutorials/*/data/* .

WORKDIR $HOME

# Testing tutorials
RUN  cp -r tutorials test_tutorials

WORKDIR $HOME/test_tutorials
# RUN jupyter nbconvert --ExecutePreprocessor.timeout=1000 --to notebook --execute "ARISE.ipynb"
# RUN jupyter nbconvert --ExecutePreprocessor.timeout=1000 --to notebook --execute "atomic_features.ipynb"
# RUN jupyter nbconvert --ExecutePreprocessor.timeout=1000 --to notebook --execute "clustering_tutorial.ipynb"
# RUN jupyter nbconvert --ExecutePreprocessor.timeout=1000 --to notebook --execute "cmlkit.ipynb"
# # RUN jupyter nbconvert --ExecutePreprocessor.timeout=1000 --to notebook --execute "CO2_SGD.ipynb"
# RUN jupyter nbconvert --ExecutePreprocessor.timeout=1000 --to notebook --execute "compressed_sensing.ipynb"
# RUN jupyter nbconvert --ExecutePreprocessor.timeout=1000 --to notebook --execute "convolutional_nn.ipynb"
# RUN jupyter nbconvert --ExecutePreprocessor.timeout=1000 --to notebook --execute "decision_tree.ipynb"
# RUN jupyter nbconvert --ExecutePreprocessor.timeout=1000 --to notebook --execute "descriptor_role.ipynb"
# # RUN jupyter nbconvert --ExecutePreprocessor.timeout=20000 --to notebook --execute "domain_of_applicability.ipynb"
# # RUN jupyter nbconvert --ExecutePreprocessor.timeout=1000 --to notebook --execute "dos_similarity_search.ipynb"
# # RUN jupyter nbconvert --ExecutePreprocessor.timeout=1000 --to notebook --execute "error_estimates.ipynb"
# RUN jupyter nbconvert --ExecutePreprocessor.timeout=1000 --to notebook --execute "exploratory_analysis.ipynb"
# # RUN jupyter nbconvert --ExecutePreprocessor.timeout=1000 --to notebook --execute "gap_si_surface.ipynb"
# # RUN jupyter nbconvert --ExecutePreprocessor.timeout=1000 --to notebook --execute "grain_boundaries.ipynb"
# # RUN jupyter nbconvert --ExecutePreprocessor.timeout=1000 --to notebook --execute "kaggle_competition.ipynb"
# RUN jupyter nbconvert --ExecutePreprocessor.timeout=1000 --to notebook --execute "krr4mat.ipynb"
# RUN jupyter nbconvert --ExecutePreprocessor.timeout=1000 --to notebook --execute "nn_regression.ipynb"
# RUN jupyter nbconvert --ExecutePreprocessor.timeout=1000 --to notebook --execute "perovskites_tolerance_factor.ipynb"
# RUN jupyter nbconvert --ExecutePreprocessor.timeout=5000 --to notebook --execute "query_nomad_archive.ipynb"
# RUN jupyter nbconvert --ExecutePreprocessor.timeout=1000 --to notebook --execute "sgd_alloys_oxygen_reduction_evolution.ipynb"
# RUN jupyter nbconvert --ExecutePreprocessor.timeout=1000 --to notebook --execute "sgd_propylene_oxidation_hte.ipynb"
# # RUN jupyter nbconvert --ExecutePreprocessor.timeout=1000 --to notebook --execute "soap_atomic_charges.ipynb"
# RUN jupyter nbconvert --ExecutePreprocessor.timeout=1000 --to notebook --execute "tcmi.ipynb"
# RUN jupyter nbconvert --ExecutePreprocessor.timeout=1000 --to notebook --execute "tetradymite_PRM2020.ipynb"

WORKDIR $HOME
RUN rm -r test_tutorials

# Fix permissions
RUN fix-permissions $TUTORIALS_HOME \
 && fix-permissions $HOME/.local/share/jupyter \
 && fix-permissions $CONDA_DIR

USER root
# ================================================================================
# Switch back to jovyan to avoid accidental container runs as root
# ================================================================================

USER $NB_UID
