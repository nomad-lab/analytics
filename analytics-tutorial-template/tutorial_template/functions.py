import pandas as pd

def print_atomic_mass_average (df):
    average = df.mean(axis=0)[1]
    print('The average atomic mass is ', average, '(g/mol)')