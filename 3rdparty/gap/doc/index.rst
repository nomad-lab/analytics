.. HQ XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
.. HQ X
.. HQ X   GAP: Gaussian Approximation Potential
.. HQ X
.. HQ X   Copyright Albert Bartok-Partay, Gabor Csanyi 2010-2019
.. HQ X
.. HQ X   gc121@cam.ac.uk
.. HQ X   
.. HQ X
.. HQ XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

.. gap documentation master file


GAP and SOAP documentation
=============================

.. module:: gap

These are the documentation pages for Gaussian Approximation Potential
(GAP) code. GAP is a plugin for QUIP, which itself can be used as a
plugin to LAMMPS or called from ASE. For installation, see the QUIP
documentation pages. The information below is on the usage of GAP.

The purpose of the GAP code is to fit interatomic potentials and then
use them for molecular simulation. 

Contents
========

.. toctree::
   :maxdepth: 1

   gap_fit.rst
   tutorials.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
* `Module Source Code <_modules/index.html>`_
