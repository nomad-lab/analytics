! HND XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
! HND X
! HND X   libAtoms+QUIP: atomistic simulation library
! HND X
! HND X   Portions of this code were written by
! HND X     Albert Bartok-Partay, Silvia Cereda, Gabor Csanyi, James Kermode,
! HND X     Ivan Solt, Wojciech Szlachta, Csilla Varnai, Steven Winfield.
! HND X
! HND X   Copyright 2006-2010.
! HND X
! HND X   Not for distribution
! HND X
! HND X   Portions of this code were written by Noam Bernstein as part of
! HND X   his employment for the U.S. Government, and are not subject
! HND X   to copyright in the USA.
! HND X
! HND X   When using this software, please cite the following reference:
! HND X
! HND X   http://www.libatoms.org
! HND X
! HND X  Additional contributions by
! HND X    Alessio Comisso, Chiara Gattinoni, and Gianpietro Moras
! HND X
! HND XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

program gap_fit_program

  use libatoms_module
  use descriptors_module
  use gp_predict_module
  use gp_fit_module
  use clustering_module
  use gap_fit_module

  implicit none

  type(gap_fit) :: main_gap_fit

  call system_initialise(verbosity=PRINT_NORMAL, enable_timing=.false.)

  call gap_fit_parse_command_line(main_gap_fit)

  call gap_fit_parse_gap_str(main_gap_fit)

  if(main_gap_fit%do_core) call read(main_gap_fit%quip_string, trim(main_gap_fit%core_param_file), keep_lf=.true.)

  call add_template_string(main_gap_fit) ! if descriptor requires a template xyz file and this is provided, write to a string and add to descriptor_str

  call read_descriptors(main_gap_fit) ! initialises descriptors from the descriptor_str and sets max_cutoff according to that.
  call read_fit_xyz(main_gap_fit)   ! reads in xyz into an array of atoms objects. sets cutoff and does calc_connect on each frame
  call print('XYZ file read')

  call get_species_xyz(main_gap_fit)  ! counts the number of species present in the xyz file.
  call add_multispecies_gaps(main_gap_fit)

  call parse_config_type_sigma(main_gap_fit)
  call parse_config_type_n_sparseX(main_gap_fit)

  if(any(main_gap_fit%add_species)) then ! descriptor_str might have changed. reinitialises descriptors from the descriptor_str and sets max_cutoff according to that.
     call read_descriptors(main_gap_fit)
  endif
  call print('Multispecies support added where requested')

  call fit_n_from_xyz(main_gap_fit) ! counts number of energies, forces, virials. computes number of descriptors and gradients.

  call set_baselines(main_gap_fit) ! sets e0 etc.

  call fit_data_from_xyz(main_gap_fit) ! converts atomic neighbourhoods (bond neighbourhoods etc.) do descriptors, and feeds those to the GP
  call print('Cartesian coordinates transformed to descriptors')

  if(main_gap_fit%sparsify_only_no_fit) then
     call system_finalise()
     stop
  end if
  
  call enable_timing()

  call system_timer('GP sparsify')

  call gp_covariance_sparse(main_gap_fit%my_gp)
  call initialise(main_gap_fit%gp_sp,main_gap_fit%my_gp)
  call gap_fit_print_xml(main_gap_fit,main_gap_fit%gp_file,main_gap_fit%sparseX_separate_file)

  call system_timer('GP sparsify')

  call system_finalise()

end program gap_fit_program
