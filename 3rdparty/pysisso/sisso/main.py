import warnings
from argparse import ArgumentParser as argpars

import dill
from time import time

from sisso.descriptor_identifcation.SISSO_regressor import SISSO_Regressor, print_models
from sisso.feature_creation.feature_space import FeatureSpace
from sisso.feature_creation.nodes.operator_nodes import op_map
from sisso.utils.mpi_interface import my_rank
from sisso.validator.validator import KFoldValidator, LeaveOutValidator

warnings.filterwarnings("ignore")


def main():
    parser = argpars(description="Run a SISSO regression ")
    parser.add_argument(
        "-d",
        "--data_file",
        type=str,
        nargs="?",
        help="file where all primary feature data is stored",
        default="data.csv",
    )
    parser.add_argument(
        "-p", "--prop_key", type=str, nargs="?", help="column key for the property"
    )
    parser.add_argument(
        "-o",
        "--ops",
        type=str,
        nargs="*",
        help="Which operators to use for creating the feature set. If all then use all available features",
        default="all",
    )
    parser.add_argument(
        "-c",
        "--cols",
        type=str,
        nargs="*",
        help="which columns to use as primary features from data_file. If 'all' passed then take all non-property columns",
        default="all",
    )
    parser.add_argument(
        "-m",
        "--max_phi",
        nargs="?",
        type=int,
        help="Maximum number of operators to apply to a set of primary features",
        default=1,
    )
    parser.add_argument(
        "-r",
        "--res_save",
        nargs="?",
        type=int,
        help="Number of residuals to save in the model",
        default=1,
    )
    parser.add_argument(
        "-cc",
        "--cross_corr_threshold",
        nargs="?",
        type=float,
        help="Cross correlation threshold to discard features during SIS",
        default=0.95,
    )
    parser.add_argument(
        "-s",
        "--n_sis_select",
        type=int,
        nargs="?",
        default=1,
        help="Maximum number of features to select for each SIS call",
    )
    parser.add_argument(
        "-n",
        "--n_dim",
        type=int,
        nargs="?",
        help="Maximum dimension model to calculate",
        default=1,
    )
    parser.add_argument(
        "-l",
        "--leave_out",
        type=float,
        nargs="?",
        help="Leave p out validation (if < 1.0 fractional)",
        default=0.0,
    )
    parser.add_argument(
        "-i",
        "--leave_out_iter",
        type=int,
        nargs="?",
        help="maximum number of interations for Leave-p-Out validation",
        default=None,
    )
    parser.add_argument(
        "-k", "--k_fold", type=int, nargs="?", help="k-fold validation", default=0
    )
    parser.add_argument(
        "--disable_all_l0_combinations",
        action="store_true",
        help="Do not combine sis selection rounds when calculating descriptors with L0",
    )
    parser.add_argument(
        "--load",
        type=str,
        nargs="?",
        help="use dill to load stored sisso object",
        default=None,
    )
    parser.add_argument(
        "-lt",
        "--learn_type",
        type=str,
        nargs="?",
        help="Objective function to learn on (log, correlation, classification)",
        default="correlation",
    )
    parser.add_argument(
        "-cw",
        "--class_width",
        type=float,
        nargs="?",
        help="Width of the boundary for classification",
        default=1e-6,
    )
    param_parser = parser.add_mutually_exclusive_group(required=False)
    param_parser.add_argument(
        "--no-parameterize",
        dest="parameterize",
        help="parameterize the nodes",
        action="store_false",
    )
    param_parser.add_argument(
        "--parameterize",
        dest="parameterize",
        help="parameterize the nodes",
        action="store_true",
    )
    parser.set_defaults(parameterize=True)
    fix_intercept = parser.add_mutually_exclusive_group(required=False)
    fix_intercept.add_argument(
        "--no_fix_intercept",
        dest="fix_intercept",
        help="Set the intercept to 0",
        action="store_false",
    )
    fix_intercept.add_argument(
        "--fix_intercept",
        dest="fix_intercept",
        help="Set the intercept to 0",
        action="store_true",
    )
    parser.set_defaults(fix_intercept=False)
    standardize = parser.add_mutually_exclusive_group(required=False)
    standardize.add_argument(
        "--no_standardize",
        dest="standardize",
        help="Set the intercept to 0",
        action="store_false",
    )
    standardize.add_argument(
        "--standardize",
        dest="standardize",
        help="Set the intercept to 0",
        action="store_true",
    )
    parser.set_defaults(standardize=False)
    log_learn = parser.add_mutually_exclusive_group(required=False)
    log_learn.add_argument(
        "--no_log_learn",
        dest="log_learn",
        help="Set the intercept to 0",
        action="store_false",
    )
    log_learn.add_argument(
        "--log_learn",
        dest="log_learn",
        help="Set the intercept to 0",
        action="store_true",
    )
    parser.set_defaults(log_learn=False)
    args = parser.parse_args()

    if args.load:
        sisso = dill.load(open(args.load, "rb"))
    else:
        allowed_ops = []
        if args.ops == "all":
            allowed_ops = list(op_map.values())
        else:
            for op in args.ops:
                allowed_ops.append(op_map[op])

        t0 = time()
        phi = FeatureSpace.from_df(
            args.data_file,
            args.prop_key,
            allowed_ops,
            args.cols,
            args.max_phi,
            args.n_sis_select,
            parameterize=args.parameterize,
            fix_c_0=args.fix_intercept,
            learn_type=args.learn_type,
            class_width=args.class_width,
        )
        # print(f"Time Feature Creation: {time()-t0}")
        t0 = time()
        sisso = SISSO_Regressor(
            phi,
            args.n_dim,
            not args.disable_all_l0_combinations,
            fix_c_0=args.fix_intercept,
            n_res_save=args.res_save,
            learn_type=args.learn_type,
        )
        # print(f"Time SISSO Regressor: {time()-t0}")
        t0 = time()
        sisso.fit()
        # print(f"Time SISSO fit: {time()-t0}")
        models = sisso.models
        if my_rank == 0:
            print("Training Results")
            print_models(models)

            for feat in sisso.feature_set.phi:
                feat._value = None
                feat._fxn_in_value = None

            with open("sisso_regressor.pick", "wb") as pickle_file:
                dill.dump(sisso, pickle_file)

    if (args.leave_out > 0) and (args.k_fold > 0):
        raise ValueError(
            "Can not do both k-Fold and Leave-p-Out validation, please specify one and load the saved sisso object"
        )
    elif args.leave_out > 0:
        if args.leave_out < 1.0:
            leave_out = None
            frac = args.leave_out
        else:
            leave_out = int(round(args.leave_out))
            frac = None

        validator = LeaveOutValidator(sisso, args.leave_out_iter, leave_out, frac)
    elif args.k_fold > 0:
        validator = KFoldValidator(sisso, args.k_fold)
    else:
        return 0

    validator.validate()
    with open("validator.pick", "wb") as pickle_file:
        dill.dump(validator, pickle_file)

    if my_rank == 0:
        av_rmse, av_max_ae = validator.summarize_error()
        print("Average prediction rmse")
        for dim, rmse in enumerate(av_rmse):
            print(f"{dim+1}D: {rmse}")
        print("Average prediction max ae")
        for dim, max_ae in enumerate(av_max_ae):
            print(f"{dim+1}D: {max_ae}")


if __name__ == "__main__":
    main()
