"""Define FeatureNode class"""

import numpy as np
import sympy

from sisso.feature_creation.nodes.node import Node
from sisso.feature_creation.nodes.unit import Unit


class FeatureNode(Node):

    """A node used to describe a primary feature for the descriptor"""

    def __init__(self, value, tag, unit=None):
        """A node used to describe a primary feature for the descriptor

        Args:
            value (number or array): The value of the feature for all materials
            tag (str): A string that represents the feature in algebraic expressions (will be converted to sympy.Symbols)
            unit (str, optional): A string that describes the unit of the feature (will be converted to sympy.Symbols)
        """
        if np.any(np.isnan(value)):
            raise ValueError("An element in the feature node is not a number")
        super(FeatureNode, self).__init__(value, tag)

        self._expr = sympy.Symbol(self._tag)
        self._n_els = len(value)

        if unit is None:
            self._unit = Unit(dict())
        else:
            self._unit = Unit.from_str(unit)

    @property
    def expr(self):
        """
        Returns:
            sympy.Symbols: The Symbol representation of tag
        """
        return self._expr

    @property
    def unit(self):
        """
        Returns:
            sympy.Symbols: The Symbol representation of the unit
        """
        return self._unit
