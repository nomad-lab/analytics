import numpy as np


def fxn_add(x, pvt):
    """Addition function"""
    return np.add(x[pvt:], x[:pvt])


def fxn_sub(x, pvt):
    """Subtraction function"""
    return np.subtract(x[pvt:], x[:pvt])


def fxn_abs_diff(x, pvt, alpha=1.0, b=1.0, c=0.0):
    """Parameterized absolute difference function"""
    return b * np.abs(np.subtract(x[pvt:], alpha * x[:pvt])) + c


def fxn_mult(x, pvt):
    """Multiplication function"""
    return np.multiply(x[pvt:], x[:pvt])


def fxn_div(x, pvt, alpha=1.0, a=0.0, c=0.0):
    """Parameterized division function"""
    return np.divide(x[pvt:], x[:pvt] * alpha + a) + c


def fxn_exp(x, alpha=1.0, a=1.0, c=0.0):
    """Parameterized exponential function"""
    return a * np.exp(alpha * x) + c


def fxn_neg_exp(x, alpha=1.0, a=1.0, c=0.0):
    """Parameterized negative exponential function"""
    return a * np.exp(-alpha * x) + c


def fxn_inv(x, alpha=1.0, a=0.0, c=0.0):
    """Parameterized inverse function"""
    return np.divide(1.0, alpha * x + a) + c


def fxn_sq(x):
    """Square function"""
    return np.power(x, 2.0)


def fxn_cb(x):
    """Cube function"""
    return np.power(x, 3.0)


def fxn_six_pwr(x):
    """Function to take the sixth power of all items in an array"""
    return np.power(x, 6.0)


def fxn_sqrt(x, alpha=1.0, a=0.0, b=1.0, c=0.0):
    """Parameterized square root function"""
    return b * np.sqrt(x * alpha + a) + c


def fxn_cbrt(x, alpha=1.0, a=0.0, c=0.0):
    """Parameterized cube root function"""
    return np.cbrt(x * alpha + a) + c


def fxn_log(x, alpha=1.0, a=0.0, c=0.0):
    """Parameterized log function"""
    return alpha * np.log(x + a) + c


def fxn_abs(x):
    """Parameterized abs function"""
    return np.abs(x)


def fxn_sin(x, alpha=1.0, a=0.0, b=1.0, c=0.0):
    """Parameterized sin function"""
    return b * np.sin(alpha * x + a) + c


def fxn_cos(x, alpha=1.0, a=0.0, b=1.0, c=0.0):
    """Parameterized cos function"""
    return b * np.cos(alpha * x + a) + c


fxn_add.name = "add"
fxn_add.default_params = {}
add = fxn_add

fxn_sub.name = "sub"
fxn_sub.default_params = {}
sub = fxn_sub

fxn_abs_diff.default_params = {"alpha": 1.0, "a": 0.0, "c": 0.0}
fxn_abs_diff.name = "abs_diff"
abs_diff = fxn_abs_diff

fxn_mult.name = "mult"
fxn_mult.default_params = {}
mult = fxn_mult

fxn_div.name = "div"
fxn_div.default_params = {"alpha": 1.0, "a": 0.0, "c": 0.0}
div = fxn_div

fxn_exp.name = "exp"
fxn_exp.default_params = {"alpha": 1.0, "a": 1.0, "c": 0.0}
exp = fxn_exp

fxn_neg_exp.name = "neg_exp"
fxn_neg_exp.default_params = {"alpha": 1.0, "a": 0.0, "c": 0.0}
neg_exp = fxn_neg_exp

fxn_inv.name = "inv"
fxn_inv.default_params = {"alpha": 1.0, "a": 0.0, "c": 0.0}
inv = fxn_inv

fxn_sq.name = "sq"
fxn_sq.default_params = {}
sq = fxn_sq

fxn_cb.name = "cb"
fxn_cb.default_params = {}
cb = fxn_cb

fxn_six_pwr.name = "six_pwr"
fxn_six_pwr.default_params = {}
six_pwr = fxn_six_pwr

fxn_sqrt.name = "sqrt"
fxn_sqrt.default_params = {"alpha": 1.0, "a": 0.0, "b": 1.0, "c": 0.0}
sqrt = fxn_sqrt

fxn_cbrt.name = "cbrt"
fxn_cbrt.default_params = {"alpha": 1.0, "a": 0.0, "c": 0.0}
cbrt = fxn_cbrt

fxn_log.name = "log"
fxn_log.default_params = {"alpha": 1.0, "a": 0.0, "c": 0.0}
log = fxn_log

fxn_abs.name = "abs"
fxn_abs.default_params = {}
abs = fxn_abs

fxn_sin.name = "sin"
fxn_sin.default_params = {"alpha": 1.0, "a": 0.0, "b": 1.0, "c": 0.0}
sin = fxn_sin

fxn_cos.name = "cos"
fxn_cos.default_params = {"alpha": 1.0, "a": 0.0, "b": 1.0, "c": 0.0}
cos = fxn_cos
