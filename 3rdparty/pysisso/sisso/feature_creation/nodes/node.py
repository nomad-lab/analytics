"""Define the base Node, FeatureNode and OperatorNode classes"""
import warnings

import numpy as np
import sympy
from scipy import optimize

from .unit import Unit

const = sympy.sympify("const")


class Node(object):

    """Base class for the tree representation of the descriptors"""

    def __init__(self, value=None, tag=None):
        """Summary

        Args:
            value (number of array, optional): Value of the node
            tag (str, optional): Expression describing the node
        """
        self._value = value
        self._tag = tag
        self._expr = None
        self._unit = None

    def __eq__(self, node):
        """Comparison operator for nodes

        Args:
            node (node.Node): Node to compare against

        Returns:
            bool: True if self.norm_value == node.norm_value
        """

        if not issubclass(type(node), Node):
            return False

        if np.allclose(self.norm_value, node.norm_value, atol=1e-8):
            return True

        return False

    def __neq__(self, node):
        """Comparison operator for nodes

        Args:
            node (node.Node): Node to compare against

        Returns:
            bool: True if self.norm_value != node.norm_value
        """
        return not self.__eq__(node)

    @property
    def value(self):
        """
        Returns:
            number or array: the numerical value of the node
        """
        return self._value

    @property
    def fxn_in_value(self):
        """Calculate the value of the node by applying _func to all attached Nodes

        Returns:
            None: The result of applying _func to attached Nodes (None since this is a virtual class with no attached feature nodes)
        """
        return self._value

    @property
    def norm_value(self):
        """
        Returns:
            array: The normalized value array
        """
        value = np.array(self.fxn_in_value)
        return value / np.linalg.norm(value)

    def isnan(self, max_val):
        """Determines if a feature is valid

        Args:
            max_val(float): maximum allowed absolute value for the feature

        Returns:
            bool: True if a feature has nan or its maximum absolute value is greater than max_val
        """

        val = self.value
        return (
            (np.isnan(np.dot(val, np.ones(val.shape))))
            or (np.abs(val).max() > max_val)
            or (val.std() < 1e-13)
        )

    @property
    def standardized_value(self):
        """
        Returns:
            array: The standardized value of the node
        """
        value = np.array(self.value).flatten()
        return (value - value.mean()) / (value.std())

    @property
    def standardized_fxn_in_value(self):
        """
        Returns:
            array: The standardized value of the node
        """
        value = np.array(self.fxn_in_value).flatten()
        value -= value.mean()
        value /= np.linalg.norm(value)
        return value

    @property
    def tag(self):
        """
        Returns:
            expression: The string/algebraic representation of the node
        """
        return self._tag

    @property
    def expr(self):
        """The sympy.Expression for the resulting feature

        Returns:
            sympy.Expression: The algebraic representation of the new feature
        """
        return self._expr

    @property
    def unit(self):
        """
        Returns:
            expression: The string/algebraic representation of the node
        """
        return self._unit

    @expr.setter
    def expr(self, expr):
        """The sympy.Expression for the resulting feature

        Returns:
            sympy.Expression: The algebraic representation of the new feature
        """
        self._expr = expr

    @property
    def fxn_in_expr(self):
        """The sympy.Expression used to generate expressions for subsequent features that use this feature

        Returns:
            sympy.Expression: The algebraic representation of the new feature with only alpha used from the params
        """
        return self.expr

    @unit.setter
    def unit(self, unit):
        """Sets the unit for a feature

        Args:
            unit (sympy.Expression): The representation of the unit
        """
        self._unit = unit

    def copy(self):
        """Copies a node

        Returns:
            to_cp(node.Node): A new Node that is the copy of the current node
        """
        to_cp = Node(value=self.value.copy(), tag=self._tag)
        to_cp.expr = self.expr
        to_cp.unit = self.unit
        return to_cp
