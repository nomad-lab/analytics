"""Defines the unit dictionary for comparisons"""

import re
import numpy as np


class Unit(dict):
    def __init__(self, dct=None):
        if dct is not None:
            for key, val in dct.items():
                self[key] = val

    @classmethod
    def from_str(cls, string):
        string = string.replace(" ", "")
        unit_comps = re.split(r"/|\*", string.replace("**", "^"))
        mult_ops = [m.start() for m in re.finditer(r"\*", string)]
        div_ops = [m.start() for m in re.finditer("/", string)]
        ops = np.hstack((np.array(mult_ops), -1.0 * np.array(div_ops)))
        ops = np.hstack((np.ones(1), np.sign(ops[np.abs(ops).argsort()])))
        dct = dict()
        for comp, op in zip(unit_comps, ops):
            type_exp = comp.split("^")
            if len(type_exp) == 1:
                dct[type_exp[0]] = float(op)
            elif len(type_exp) == 2:
                dct[type_exp[0]] = int(type_exp[1]) * float(op)
            else:
                raise ValueError("Invalid unit")

        return cls(dct)

    def __repr__(self):
        string = ""
        for key, val in self.items():
            string += " * " + key + "^" + str(val)
        return string[3:]

    def __mul__(self, unit_2):
        dct = self.copy()
        for key, val in unit_2.items():
            dct[key] = dct.get(key, 0) + val
            if dct[key] == 0:
                del dct[key]
        return Unit(dct)

    def __truediv__(self, unit_2):
        dct = self.copy()
        for key, val in unit_2.items():
            dct[key] = dct.get(key, 0) - val
            if dct[key] == 0:
                del dct[key]
        return Unit(dct)

    def __pow__(self, power):
        dct = self.copy()
        for key, val in dct.items():
            dct[key] = val * power
        return Unit(dct)

    def inv(self):
        dct = self.copy()
        for key, val in dct.items():
            dct[key] = val * -1.0
        return Unit(dct)
