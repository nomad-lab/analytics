"""File to describe SquareNode"""

import sisso.feature_creation.nodes.functions as fxn
import sympy
from sisso.feature_creation.nodes.operator_node import OperatorNode


class SqNode(OperatorNode):

    """Node to add square operators to features
    """

    def __init__(self, feat):
        """Node to add square operators to features

        Args:
            feat (Node): Feature to add the square operator to
        Raises:
            ValueError: If feature would leave accepted range
        """

        if feat.tag == "sqrt":
            raise ValueError("Invalid feature combination")

        super(SqNode, self).__init__(fxn.sq, "**2", feat)

    @property
    def expr(self):
        """The sympy.Expression for the resulting feature

        Returns:
            sympy.Expression: The algebraic representation of the new feature
        """
        return sympy.expand(
            sympy.trigsimp(sympy.powdenest(self.feat.fxn_in_expr ** 2, force=True))
        )

    # @property
    def get_unit(self):
        """The sympy.Expression for the unit of the resulting feature

        Returns:
            sympy.Expression: The resulting unit of the feature
        """
        return self.feat.unit ** 2.0
