"""File to describe CbrtNode"""

import numpy as np
import sisso.feature_creation.nodes.functions as fxn
import sympy
from scipy import stats
from sisso.feature_creation.nodes.operator_node import OperatorNode
from sisso.feature_creation.nodes.operator_nodes.derivatives import get_deriv


class CbrtNode(OperatorNode):

    """Node to add cube root operators to features
    """

    def __init__(self, feat):
        """Node to add cube root operators to features

        Args:
            feat (Node): Feature to add the cube root operator to
        """
        disallowed = ["**3", "**6"]
        if feat.tag in disallowed:
            raise ValueError("Invalid feature combination")

        super(CbrtNode, self).__init__(fxn.cbrt, "cbrt", feat)
        self.bounds = (-1.0 * np.inf, np.inf)

    def initial_params(self, prop, mat_inds):
        """Get an initial estimate of the parameters

        Args:
            prop (np.ndarray(float)): Property to fit to
            mat_inds (np.ndarray(int)): Indexes to include in the fitting

        Returns:
            dict: The initial parameter guess based on the property and self.feat.fxn_in_value
        """
        val = self.feat.fxn_in_value[mat_inds]

        # Get the sign of the alpha parameter
        alpha_sign = float(np.sign(stats.linregress(val, prop[mat_inds])[0]))

        # Get and Transform the derivative of prop with respect to feat
        x, prop_prim = get_deriv(val, prop[mat_inds], 1)
        prop_trans = np.abs(prop_prim) ** (-1.5)

        # Check if \alpha * a is in the Domain if Not do normal analysis
        if np.max(np.abs(prop_prim)) > 1.0:
            ind_max = np.abs(prop_prim).argmax()
            x = x[ind_max:]
            prop_trans = prop_trans[ind_max:]

        # Disregard any strongly non-linear trends due to noise/outliers
        threshold = np.median(np.abs(np.diff(prop_trans))) * 1.1
        inds = np.where(np.abs(np.diff(prop_trans)) <= threshold)[0]

        if np.max(np.abs(prop_prim)) > 1.0:
            # Get alpha from the slope of the line
            alpha = stats.linregress(x[inds], prop_trans[inds])[0]
            alpha = alpha_sign * 27.0 / alpha ** 2.0

            # Calculate a from the location of the max value of prop_prim and alpha
            a = -1.0 * x[0] * alpha
        else:
            # Get the sign of a
            a_sign = -1.0 * float(np.sign(stats.linregress(x, prop_prim)[0]))

            # Get initial parameter guess
            alpha, a = stats.linregress(x[inds], prop_trans[inds])[:2]

            # Correct alpha and a
            alpha = alpha_sign * 27.0 / alpha ** 2.0
            a *= a_sign * (np.abs(alpha) / 3.0) ** (1.5)

        # Get an estimate of the constant shift and scale factor
        c = stats.linregress(np.cbrt(alpha * val + a), prop[mat_inds])[1]

        return {"alpha": alpha, "a": a, "c": c}

    @property
    def fxn_in_value(self):
        """Calculate the value of the node by applying _func to feat_1 and feat_2 in that order

        Returns:
            np.ndarray: The result of applying _func to attached Nodes, with only alpha set the correct value
        """

        if self._fxn_in_value is not None:
            return self._fxn_in_value

        return self._func(self.feat.fxn_in_value)

    def set_fxn_in_value(self):
        """Sets the value array of the feature based off the function/feat.fxn_in_value"""
        self._fxn_in_value = self._func(self.feat.fxn_in_value)

    @property
    def expr(self):
        """The sympy.Expression for the resulting feature

        Returns:
            sympy.Expression: The algebraic representation of the new feature
        """
        return sympy.trigsimp(
            sympy.powdenest(
                sympy.cbrt(
                    self.params["alpha"] * self.feat.fxn_in_expr + self.params["a"]
                ),
                force=True,
            )
            + self.params["c"]
        )

    @property
    def fxn_in_expr(self):
        """The sympy.Expression used to generate expressions for subsequent features that use this feature

        Returns:
            sympy.Expression: The algebraic representation of the new feature with only alpha used from the params
        """
        return sympy.trigsimp(
            sympy.powdenest(sympy.cbrt(self.feat.fxn_in_expr), force=True)
        )

    # @property
    def get_unit(self):
        """The sympy.Expression for the unit of the resulting feature

        Returns:
            sympy.Expression: The resulting unit of the feature
        """
        return self.feat.unit ** (1.0 / 3.0)
