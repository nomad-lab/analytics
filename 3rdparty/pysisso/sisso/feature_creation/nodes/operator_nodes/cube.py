"""File to describe CubeNode"""

import sisso.feature_creation.nodes.functions as fxn
import sympy
from sisso.feature_creation.nodes.operator_node import OperatorNode


class CubNode(OperatorNode):

    """Node to add cube operators to features
    """

    def __init__(self, feat):
        """Node to add cube operators to features

        Args:
            feat (Node): Feature to add the cube operator to
        Raises:
            ValueError: If feature would leave accepted range
        """
        if feat.tag == "cbrt":
            raise ValueError("Invalid feature combination")

        super(CubNode, self).__init__(fxn.cb, "**3", feat)

    @property
    def expr(self):
        """The sympy.Expression for the resulting feature

        Returns:
            sympy.Expression: The algebraic representation of the new feature
        """
        return sympy.expand(
            sympy.trigsimp(sympy.powdenest(self.feat.fxn_in_expr ** 3, force=True))
        )

    # @property
    def get_unit(self):
        """The sympy.Expression for the unit of the resulting feature

        Returns:
            sympy.Expression: The resulting unit of the feature
        """
        return self.feat.unit ** 3.0
