"""File to describe InverseNode"""

import numpy as np
import sisso.feature_creation.nodes.functions as fxn
import sympy
from scipy import stats
from sisso.feature_creation.nodes.operator_node import OperatorNode
from sisso.feature_creation.nodes.operator_nodes.derivatives import get_deriv


class InvNode(OperatorNode):

    """Node to add inverse operators to features
    """

    def __init__(self, feat):
        """Node to add inverse operators to features

        Args:
            feat (Node): Feature to add the inverse operator to
        Raises:
            ValueError: If feature would leave accepted range
        """
        disallowed = ["exp", "exp(-)", "log", "inv"]
        if feat.tag in disallowed:
            raise ValueError("Invalid feature combination")

        super(InvNode, self).__init__(fxn.inv, "inv", feat)
        self.bounds = (-1.0 * np.inf, np.inf)

    def initial_params(self, prop, mat_inds):
        """Get an initial estimate of the parameters

        Args:
            prop (np.ndarray(float)): Property to fit to
            mat_inds (np.ndarray(int)): Indexes to include in the fitting

        Returns:
            dict: The initial parameter guess based on the property and self.feat.fxn_in_value
        """
        val = self.feat.fxn_in_value[mat_inds]

        # Get and Transform the derivative of prop with respect to feat
        x, prop_prim = get_deriv(val, prop[mat_inds], 1)
        prop_trans = 1.0 / np.sqrt(np.abs(prop_prim))

        # Check for the asymtope
        if np.abs(prop_prim).max() > 10.0:
            inds_stop = np.where(np.abs(prop_prim) > 10.0)[0]
            a_alpha = np.mean(x[inds_stop])
            prop_trans = prop_trans[: inds_stop[0]]
            x = x[: inds_stop[0]]

            alpha_sign = 1.0
            if val[prop[mat_inds].argmin()] < val[prop[mat_inds].argmax()]:
                alpha_sign = -1.0

        # Disregard any strongly non-linear trends due to noise/outliers
        threshold = np.median(np.abs(np.diff(prop_trans))) * 1.1
        inds = np.where(np.abs(np.diff(prop_trans)) <= threshold)[0]

        if np.abs(prop_prim).max() > 10.0:
            alpha = alpha_sign * stats.linregress(x[inds], prop_trans[inds])[0]
            a = -1.0 * a_alpha * alpha
            c = np.abs(prop[mat_inds]).min()
        else:
            # Get initial parameter guess
            alpha, a = stats.linregress(x[inds], prop_trans[inds])[:2]
            # Correct alpha and a
            alpha = 2.0 * alpha ** 3.0
            a *= (2.0 * alpha ** 2.0) ** (1.0 / 3.0)

            # Get an estimate of the constant shift
            c = stats.linregress(1.0 / (alpha * val + a), prop[mat_inds])[1]

        return {"alpha": alpha, "a": a, "c": c}

    @property
    def fxn_in_value(self):
        """Calculate the value of the node by applying _func to feat_1 and feat_2 in that order

        Returns:
            np.ndarray: The result of applying _func to attached Nodes, with only alpha set the correct value
        """

        if self._fxn_in_value is not None:
            return self._fxn_in_value

        return self._func(self.feat.fxn_in_value)

    def set_fxn_in_value(self):
        """Sets the value array of the feature based off the function/feat.fxn_in_value"""
        self._fxn_in_value = self._func(self.feat.fxn_in_value)

    @property
    def expr(self):
        """The sympy.Expression for the resulting feature

        Returns:
            sympy.Expression: The algebraic representation of the new feature
        """
        return (
            1 / (self.params["alpha"] * self.feat.fxn_in_expr + self.params["a"])
            + self.params["c"]
        )

    @property
    def fxn_in_expr(self):
        """The sympy.Expression used to generate expressions for subsequent features that use this feature

        Returns:
            sympy.Expression: The algebraic representation of the new feature with only alpha used from the params
        """
        return sympy.trigsimp(sympy.cancel(1 / self.feat.fxn_in_expr))

    # @property
    def get_unit(self):
        """The sympy.Expression for the unit of the resulting feature

        Returns:
            sympy.Expression: The resulting unit of the feature
        """
        return self.feat.unit.inv()
