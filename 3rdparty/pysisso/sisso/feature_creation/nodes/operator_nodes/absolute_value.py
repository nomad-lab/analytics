"""File to describe AbsNode"""

import numpy as np
import sisso.feature_creation.nodes.functions as fxn
import sympy
from sisso.feature_creation.nodes.operator_node import OperatorNode


class AbsNode(OperatorNode):

    """Node to add absolute value operators to features
    """

    def __init__(self, feat):
        """Node to add absolute value operators to features

        Args:
            feat (Node): Feature to add the absolute value operator to
        Raises:
            ValueError: If feat.value is strictly not-negative
        """
        if np.sign(np.min(feat.value)) >= 0:
            raise ValueError("Absoulte value will produce the same feature")

        super(AbsNode, self).__init__(fxn.abs, "abs", feat)

    @property
    def expr(self):
        """The sympy.Expression for the resulting feature

        Returns:
            sympy.Expression: The algebraic representation of the new feature
        """
        return sympy.Abs(self.feat.fxn_in_expr)

    # @property
    def get_unit(self):
        """The sympy.Expression for the unit of the resulting feature

        Returns:
            sympy.Expression: The resulting unit of the feature
        """
        return self.feat.unit
