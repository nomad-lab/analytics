"""File to describe ExpNode and NegExpNode"""

import numpy as np
import sisso.feature_creation.nodes.functions as fxn
import sympy
from scipy import stats
from sisso.feature_creation.nodes.operator_node import OperatorNode
from sisso.feature_creation.nodes.operator_nodes.derivatives import get_deriv
from sisso.feature_creation.nodes.unit import Unit


class ExpNode(OperatorNode):

    """Node to add exp operators to features
    """

    def __init__(self, feat):
        """Node to add exp operators to features

        Args:
            feat (Node): Feature to add the exp operator to

        Raises:
            ValueError: If feat is not unitless or feature would leave accepted range
        """
        disallowed = ["exp", "exp(-)", "log", "sin", "cos"]
        if feat.tag in disallowed:
            raise ValueError("Invalid feature combination")

        super(ExpNode, self).__init__(fxn.exp, "exp", feat)
        self.bounds = ([0.0, -np.inf, -np.inf], np.inf)

    def initial_params(self, prop, mat_inds):
        """Get an initial estimate of the parameters

        Args:
            prop (np.ndarray(float)): Property to fit to
            mat_inds (np.ndarray(int)): Indexes to include in the fitting

        Returns:
            dict: The initial parameter guess based on the property and self.feat.fxn_in_value
        """

        val = self.feat.fxn_in_value[mat_inds]

        # Determine the sign of the prefactor
        a_sign = float(np.sign(stats.linregress(val, prop[mat_inds])[0]))

        # Get and Transform the derivative of prop with respect to feat
        x, prop_prim = get_deriv(val, prop[mat_inds])
        prop_trans = np.log(np.abs(prop_prim))

        # Disregard any strongly non-linear trends due to noise/outliers
        threshold = np.median(np.abs(np.diff(prop_trans))) * 1.1
        inds = np.where(np.abs(np.diff(prop_trans)) <= threshold)[0]

        # Get initial parameter guess
        alpha, a = stats.linregress(x[inds], prop_trans[inds])[:2]

        if alpha < 0.0:
            raise ValueError("Data suggests negative exponential")

        # Reformat a to be the right value
        a = np.exp(a - np.log(alpha)) * a_sign

        # Get an estimate of the constant shift
        c = stats.linregress(a * np.exp(alpha * val), prop[mat_inds])[1]

        return {"alpha": alpha, "a": a, "c": c}

    @property
    def fxn_in_value(self):
        """Calculate the value of the node by applying _func to feat_1 and feat_2 in that order

        Returns:
            np.ndarray: The result of applying _func to attached Nodes, with only alpha set the correct value
        """

        if self._fxn_in_value is not None:
            return self._fxn_in_value

        return self._func(self.feat.fxn_in_value, self.params["alpha"])

    def set_fxn_in_value(self):
        """Sets the value array of the feature based off the function/feat.fxn_in_value"""
        self._fxn_in_value = self._func(self.feat.fxn_in_value, self.params["alpha"])

    @property
    def expr(self):
        """The sympy.Expression for the resulting feature

        Returns:
            sympy.Expression: The algebraic representation of the new feature
        """
        return self.params["c"] + self.params["a"] * sympy.powdenest(
            sympy.exp(self.params["alpha"] * self.feat.fxn_in_expr), force=True
        )

    @property
    def fxn_in_expr(self):
        """The sympy.Expression used to generate expressions for subsequent features that use this feature

        Returns:
            sympy.Expression: The algebraic representation of the new feature with only alpha used from the params
        """
        return sympy.powdenest(
            sympy.exp(self.params["alpha"] * self.feat.fxn_in_expr), force=True
        )

    # @property
    def get_unit(self):
        """The sympy.Expression for the unit of the resulting feature

        Returns:
            sympy.Expression: The resulting unit of the feature
        """
        return Unit()


class NegExpNode(OperatorNode):

    """Node to add the negative exponential operators to features
    """

    def __init__(self, feat):
        """Node to add the negative exponential operators to features

        Args:
            feat (Node): Feature to add the the negative exponential operator to

        Raises:
            ValueError: If feat is not unitless or feature would leave accepted range
        """
        disallowed = ["exp", "exp(-)", "log", "sin", "cos"]
        if feat.tag in disallowed:
            raise ValueError("Invalid feature combination")

        super(NegExpNode, self).__init__(fxn.neg_exp, "exp(-)", feat)

        self.bounds = ([0.0, -np.inf, -np.inf], np.inf)

    def initial_params(self, prop, mat_inds):
        """Get an initial estimate of the parameters

        Args:
            prop (np.ndarray(float)): Property to fit to
            mat_inds (np.ndarray(int)): Indexes to include in the fitting

        Returns:
            dict: The initial parameter guess based on the property and self.feat.fxn_in_value
        """
        val = self.feat.fxn_in_value[mat_inds]

        # Determine the sign of the prefactor
        a_sign = -1.0 * float(np.sign(stats.linregress(val, prop[mat_inds])[0]))

        # Get and Transform the derivative of prop with respect to feat
        x, prop_prim = get_deriv(val, prop[mat_inds])
        prop_trans = np.log(np.abs(prop_prim))

        # Disregard any strongly non-linear trends due to noise/outliers
        threshold = np.median(np.abs(np.diff(prop_trans))) * 1.1
        inds = np.where(np.abs(np.diff(prop_trans)) <= threshold)[0]

        # Get initial parameter guess
        alpha, a = stats.linregress(-1.0 * x[inds], prop_trans[inds])[:2]

        if alpha < 0.0:
            raise ValueError("Data suggests non-negative exponential")

        # Reformat a to be the right value
        a = np.exp(a - np.log(alpha)) * a_sign

        # Get an estimate of the constant shift
        c = stats.linregress(a * np.exp(-1.0 * alpha * val), prop[mat_inds])[1]

        return {"alpha": alpha, "a": a, "c": c}

    @property
    def expr(self):
        """The sympy.Expression for the resulting feature

        Returns:
            sympy.Expression: The algebraic representation of the new feature
        """
        return self.params["c"] + self.params["a"] * sympy.powdenest(
            sympy.exp(-1 * self.params["alpha"] * self.feat.fxn_in_expr), force=True
        )

    @property
    def fxn_in_value(self):
        """Calculate the value of the node by applying _func to feat_1 and feat_2 in that order

        Returns:
            np.ndarray: The result of applying _func to attached Nodes, with only alpha set the correct value
        """

        if self._fxn_in_value is not None:
            return self._fxn_in_value

        return self._func(self.feat.fxn_in_value, self.params["alpha"])

    def set_fxn_in_value(self):
        """Sets the value array of the feature based off the function/feat.fxn_in_value"""
        self._fxn_in_value = self._func(self.feat.fxn_in_value, self.params["alpha"])

    @property
    def fxn_in_expr(self):
        """The sympy.Expression used to generate expressions for subsequent features that use this feature

        Returns:
            sympy.Expression: The algebraic representation of the new feature with only alpha used from the params
        """
        return sympy.powdenest(
            sympy.exp(-1 * self.params["alpha"] * self.feat.fxn_in_expr), force=True
        )

    # @property
    def get_unit(self):
        """The sympy.Expression for the unit of the resulting feature

        Returns:
            sympy.Expression: The resulting unit of the feature
        """
        return Unit()
