"""File to describe MultNode"""

import sisso.feature_creation.nodes.functions as fxn
from sisso.feature_creation.nodes.operator_node import OperatorNode


class MultNode(OperatorNode):

    """Node to add Mult operators to the features
    """

    def __init__(self, feat_1, feat_2):
        """Node to add Mult operators to the features

        Args:
            feat_1 (Node): Feature on the left of the Mult
            feat_2 (Node): Feature on the right of the Mult
        Raises:
            ValueError: If feature would leave accepted range
        """

        func = lambda x: fxn.mult(x, self._n_els)
        func.name = "mult"
        func.default_params = {}
        super(MultNode, self).__init__(func, "*", [feat_1, feat_2])

    @property
    def expr(self):
        """The sympy.Expression for the resulting feature

        Returns:
            sympy.Expression: The algebraic representation of the new feature
        """
        new_expr = self.feats[0].fxn_in_expr * self.feats[1].fxn_in_expr
        return new_expr

    # @property
    def get_unit(self):
        """The sympy.Expression for the unit of the resulting feature

        Returns:
            sympy.Expression: The resulting unit of the feature
        """
        return self.feats[0].unit * self.feats[1].unit

    def reset_pivot(self, pvt):
        """reset the divider for self._func"""
        self._func = lambda x: fxn.mult(x, pvt)
        self._func.name = "mult"
        self._func.default_params = {}
