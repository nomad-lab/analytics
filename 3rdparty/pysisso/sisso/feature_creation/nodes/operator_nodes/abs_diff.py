"""File to describe AbsDiffNode"""

import numpy as np
import sisso.feature_creation.nodes.functions as fxn
import sympy
from sisso.feature_creation.nodes.operator_node import OperatorNode


class AbsDiffNode(OperatorNode):

    """Node to add absolute difference operators to the features
    """

    def __init__(self, feat_1, feat_2):
        """Node to add absolute difference operators to the features

        Args:
            feat_1 (Node): Feature on the left of the absolute difference
            feat_2 (Node): Feature on the right of the absolute difference

        Raises:
            ValueError: If the unit for feat_1 and feat_2 are not the same or the resulting feature would be constant
        """
        if feat_1.unit != feat_2.unit:
            raise ValueError("When subtracting both features must have the same units")

        if feat_1 == feat_2:
            raise ValueError("When subtracting both features must be different")

        func = lambda x, alpha=1.0, b=1.0, c=0.0: fxn.abs_diff(
            x, self._n_els, alpha, b, c
        )
        func.default_params = {"alpha": 1.0, "b": 1.0, "c": 0.0}
        func.name = "abs_diff"

        super(AbsDiffNode, self).__init__(func, "|-|", [feat_1, feat_2])
        self.bounds = ([0.0, -np.inf, -np.inf], np.inf)

    def initial_params(self, prop, mat_inds):
        """Get an initial estimate of the parameters

        Args:
            prop (np.ndarray(float)): Property to fit to
            mat_inds (np.ndarray(int)): Indexes to include in the fitting

        Returns:
            dict: The initial parameter guess (standard approximation as no good way to guess)
        """
        return {"alpha": 1.0, "b": 1.0, "c": 0.0}

    @property
    def fxn_in_value(self):
        """Calculate the value of the node by applying _func to feat_1 and feat_2 in that order

        Returns:
            np.ndarray: The result of applying _func to attached Nodes, with only alpha set the correct value
        """

        if self._fxn_in_value is not None:
            return self._fxn_in_value

        return self._func(
            np.hstack([f.fxn_in_value for f in self.feats]), self.params["alpha"]
        )

    def set_fxn_in_value(self):
        """Sets the value array of the feature based off the function/feat.fxn_in_value"""
        self._value = self._func(
            np.hstack([f.fxn_in_value for f in self.feats]), self.params["alpha"]
        )

    @property
    def expr(self):
        """The sympy.Expression for the resulting feature

        Returns:
            sympy.Expression: The algebraic representation of the new feature
        """
        new_expr = self.params["b"] * sympy.Abs(
            (self.feats[0].fxn_in_expr)
            - self.params["alpha"] * (self.feats[1].fxn_in_expr)
        )
        return sympy.trigsimp(new_expr)

    @property
    def fxn_in_expr(self):
        """The sympy.Expression used to generate expressions for subsequent features that use this feature

        Returns:
            sympy.Expression: The algebraic representation of the new feature with only alpha used from the params
        """
        new_expr = sympy.Abs(
            (self.feats[0].fxn_in_expr)
            - self.params["alpha"] * (self.feats[1].fxn_in_expr)
        )
        return sympy.trigsimp(new_expr)

    # @property
    def get_unit(self):
        """The sympy.Expression for the unit of the resulting feature

        Returns:
            sympy.Expression: The resulting unit of the feature
        """
        return self.feats[0].unit

    def reset_pivot(self, pvt):
        """reset the divider for self._func"""
        self._func = lambda x, alpha=1.0, b=1.0, c=0.0: fxn.abs_diff(
            x, pvt, alpha, a, c
        )
        self._func.default_params = {"alpha": 1.0, "b": 1.0, "c": 0.0}
        self._func.name = "abs_diff"
