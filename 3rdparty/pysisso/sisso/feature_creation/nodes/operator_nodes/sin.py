"""File to describe SinNode"""

import numpy as np
import sisso.feature_creation.nodes.functions as fxn
import sympy
from scipy import integrate
from sisso.feature_creation.nodes.operator_node import OperatorNode

from sisso.feature_creation.nodes.unit import Unit


class SinNode(OperatorNode):

    """Node to add sin operators to features
    """

    def __init__(self, feat):
        """Node to add sin operators to features

        Args:
            feat (Node): Feature to add the sin operator to

        Raises:
            ValueError: If feat is not unitless
        """
        disallowed = ["exp", "exp(-)", "log", "sin", "cos"]
        if feat.tag in disallowed:
            raise ValueError("Invalid feature combination")

        super(SinNode, self).__init__(fxn.sin, "sin", feat)

        self.bounds = (-np.inf, np.inf)

    def initial_params(self, prop, mat_inds):
        """Get an initial estimate of the parameters

        Args:
            prop (np.ndarray(float)): Property to fit to
            mat_inds (np.ndarray(int)): Indexes to include in the fitting

        Returns:
            dict: The initial parameter guess based on the property and self.feat.fxn_in_value
        """
        val = self.feat.fxn_in_value[mat_inds]

        # Get estimates for b and c from the mean and range of prop
        b = (prop[mat_inds].max() - prop[mat_inds].min()) / 2.0
        c = prop[mat_inds].mean()

        # Rescale prop and remove outliers
        prop_scaled = (prop[mat_inds] - c) / b
        prop_scaled[np.where(prop_scaled > 1)] = 1
        prop_scaled[np.where(prop_scaled < -1)] = -1

        inds = val.argsort()
        x = val[inds]
        y = prop_scaled[inds]
        # Take the cumulative of prop_scaled as an approximation
        int_y = integrate.cumtrapz(y, x, initial=0.0)
        alpha = 2.0 / (np.max(int_y) - np.min(int_y))
        cos_cent = 1.0 / alpha * np.sin(alpha * x)
        a = (x[np.argmin(int_y)] - x[np.argmin(cos_cent)]) * (-alpha)
        return {"alpha": alpha, "a": a, "b": b, "c": c}

    @property
    def expr(self):
        """The sympy.Expression for the resulting feature

        Returns:
            sympy.Expression: The algebraic representation of the new feature
        """
        return (
            self.params["b"]
            * sympy.trigsimp(
                sympy.sin(
                    self.params["alpha"] * self.feat.fxn_in_expr + self.params["a"]
                )
            )
            + self.params["c"]
        )

    @property
    def fxn_in_expr(self):
        """The sympy.Expression for the resulting feature

        Returns:
            sympy.Expression: The algebraic representation of the new feature
        """
        return sympy.trigsimp(
            sympy.sin(self.params["alpha"] * self.feat.fxn_in_expr + self.params["a"])
        )

    # @property
    def get_unit(self):
        """The sympy.Expression for the unit of the resulting feature

        Returns:
            sympy.Expression: The resulting unit of the feature
        """
        return Unit()
