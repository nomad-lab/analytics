"""File to describe LogNode"""

import numpy as np
import sisso.feature_creation.nodes.functions as fxn
import sympy
from scipy import stats
from sisso.feature_creation.nodes.operator_node import OperatorNode
from sisso.feature_creation.nodes.operator_nodes.derivatives import get_deriv
from sisso.feature_creation.nodes.unit import Unit


class LogNode(OperatorNode):

    """Node to add log operators to features
    """

    def __init__(self, feat):
        """Node to add log operators to features

        Args:
            feat (Node): Feature to add the log operator to

        Raises:
            ValueError: If feat is not unitless or feat.value < 1/max_abs_feat_val
        """
        disallowed = ["exp", "exp(-)", "log", "sin", "cos", "inv"]
        if feat.tag in disallowed:
            raise ValueError("Invalid feature combination")

        super(LogNode, self).__init__(fxn.log, "log", feat)
        self.bounds = (-np.inf, np.inf)

    def initial_params(self, prop, mat_inds):
        """Get an initial estimate of the parameters

        Args:
            prop (np.ndarray(float)): Property to fit to
            mat_inds (np.ndarray(int)): Indexes to include in the fitting

        Returns:
            dict: The initial parameter guess based on the property and self.feat.fxn_in_value
        """
        val = self.feat.fxn_in_value[mat_inds]

        # Get and Transform the derivative of prop with respect to feat
        x, prop_prim = get_deriv(val, prop[mat_inds], 1)
        prop_trans = (prop_prim) ** (-1.0)

        # Disregard any strongly non-linear trends due to noise/outliers
        threshold = np.median(np.abs(np.diff(prop_trans))) * 1.1
        inds = np.where(np.abs(np.diff(prop_trans)) <= threshold)[0]

        # Get initial parameter guess
        alpha, a = stats.linregress(x[inds], prop_trans[inds])[:2]

        # Correct alpha and a
        alpha = 1.0 / alpha
        a *= alpha

        # Get an estimate of the constant shift and scale factor
        c = stats.linregress(alpha * np.log(val + a), prop[mat_inds])[1]

        return {"alpha": alpha, "a": a, "c": c}

    @property
    def fxn_in_value(self):
        """Calculate the value of the node by applying _func to feat_1 and feat_2 in that order

        Returns:
            np.ndarray: The result of applying _func to attached Nodes, with only alpha set the correct value
        """

        if self._fxn_in_value is not None:
            return self._fxn_in_value

        return self._func(self.feat.fxn_in_value)

    def set_fxn_in_value(self):
        """Sets the value array of the feature based off the function/feat.fxn_in_value"""
        self._fxn_in_value = self._func(self.feat.fxn_in_value)

    @property
    def expr(self):
        """The sympy.Expression for the resulting feature

        Returns:
            sympy.Expression: The algebraic representation of the new feature
        """
        return self.params["c"] + self.params["alpha"] * sympy.logcombine(
            sympy.log(self.params["a"] + self.feat.fxn_in_expr), force=True
        )

    @property
    def fxn_in_expr(self):
        """The sympy.Expression used to generate expressions for subsequent features that use this feature

        Returns:
            sympy.Expression: The algebraic representation of the new feature with only alpha used from the params
        """
        return sympy.logcombine(sympy.log(self.feat.fxn_in_expr), force=True)

    # @property
    def get_unit(self):
        """The sympy.Expression for the unit of the resulting feature

        Returns:
            sympy.Expression: The resulting unit of the feature
        """
        return Unit()
