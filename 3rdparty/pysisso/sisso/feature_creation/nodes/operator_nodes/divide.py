"""File to describe DivNode"""

import numpy as np
import sisso.feature_creation.nodes.functions as fxn
import sympy
from scipy import stats
from sisso.feature_creation.nodes.operator_node import OperatorNode
from sisso.feature_creation.nodes.operator_nodes.derivatives import get_deriv


class DivNode(OperatorNode):

    """Node to add Div operators to the features
    """

    def __init__(self, feat_1, feat_2):
        """Node to add Div operators to the features

        Args:
            feat_1 (Node): Feature on the left of the Div
            feat_2 (Node): Feature on the right of the Div

        Raises:
            ValueError: If feature would leave accepted range
        """
        func = lambda x, alpha=1.0, a=0.0, c=0.0: fxn.div(x, self._n_els, alpha, a, c)
        func.name = "div"
        func.default_params = {"alpha": 1.0, "a": 0.0, "c": 0.0}
        super(DivNode, self).__init__(func, "/", [feat_1, feat_2])
        self.bounds = (-1.0 * np.inf, np.inf)

    def initial_params(self, prop, mat_inds):
        """Get an initial estimate of the parameters

        Args:
            prop (np.ndarray(float)): Property to fit to
            mat_inds (np.ndarray(int)): Indexes to include in the fitting

        Returns:
            dict: The initial parameter guess based on the property and self.feat.fxn_in_value
        """
        val_1 = self.feats[0].fxn_in_value[mat_inds]
        val_2 = self.feats[1].fxn_in_value[mat_inds]

        # Get and Transform the derivative of prop with respect to feat
        x, prop_prim = get_deriv(val_2, val_1 * prop[mat_inds], 1)
        prop_trans = 1.0 / np.sqrt(np.abs(prop_prim))

        # Check for the asymtope
        if np.abs(prop_prim).max() > 10.0:
            inds_stop = np.where(np.abs(prop_prim) > 10.0)[0]
            a_alpha = np.mean(x[inds_stop])
            prop_trans = prop_trans[: inds_stop[0]]
            x = x[: inds_stop[0]]
            alpha_sign = 1.0
            if val_2[prop[mat_inds].argmin()] < val_2[prop[mat_inds].argmax()]:
                alpha_sign = -1.0

        # Disregard any strongly non-linear trends due to noise/outliers
        threshold = np.median(np.abs(np.diff(prop_trans))) * 1.1
        inds = np.where(np.abs(np.diff(prop_trans)) <= threshold)[0]

        if np.abs(prop_prim).max() > 10.0:
            alpha = alpha_sign * stats.linregress(x[inds], prop_trans[inds])[0]
            a = -1.0 * a_alpha * alpha
            c = np.abs(val_1 / prop[mat_inds]).min()
        else:
            # Get initial parameter guess
            alpha, a = stats.linregress(x[inds], prop_trans[inds])[:2]
            # Correct alpha and a
            alpha = 2.0 * alpha ** 3.0
            a *= (2.0 * alpha ** 2.0) ** (1.0 / 3.0)

            # Get an estimate of the constant shift
            c = stats.linregress(val_1 / (alpha * val_2 + a), prop[mat_inds])[1]

        return {"alpha": alpha, "a": a, "c": c}

    @property
    def fxn_in_value(self):
        """Calculate the value of the node by applying _func to feat_1 and feat_2 in that order

        Returns:
            np.ndarray: The result of applying _func to attached Nodes, with only alpha set the correct value
        """

        if self._fxn_in_value is not None:
            return self._fxn_in_value

        return self._func(np.hstack([f.fxn_in_value for f in self.feats]))

    def set_fxn_in_value(self):
        """Sets the value array of the feature based off the function/feat.fxn_in_value"""
        self._fxn_in_value = self._func(np.hstack([f.fxn_in_value for f in self.feats]))

    @property
    def expr(self):
        """The sympy.Expression for the resulting feature

        Returns:
            sympy.Expression: The algebraic representation of the new feature
        """
        new_expr = (self.feats[0].fxn_in_expr) / (
            self.params["alpha"] * self.feats[1].fxn_in_expr + self.params["a"]
        )
        return new_expr + self.params["c"]

    @property
    def fxn_in_expr(self):
        """The sympy.Expression used to generate expressions for subsequent features that use this feature

        Returns:
            sympy.Expression: The algebraic representation of the new feature with only alpha used from the params
        """
        new_expr = self.feats[0].fxn_in_expr / self.feats[1].fxn_in_expr
        return sympy.trigsimp(sympy.cancel(sympy.expand(new_expr)))

    def get_unit(self):
        """The sympy.Expression for the unit of the resulting feature

        Returns:
            sympy.Expression: The resulting unit of the feature
        """
        return self.feats[0].unit / self.feats[1].unit

    def reset_pivot(self, pvt):
        """reset the divider for self._func"""
        self._func = lambda x, alpha=1.0, a=0.0, c=0.0: fxn.div(x, pvt, alpha, a, c)
        self._func.name = "div"
        self._func.default_params = {"alpha": 1.0, "a": 0.0, "c": 0.0}
