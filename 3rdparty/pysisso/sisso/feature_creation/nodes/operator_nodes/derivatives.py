"""Get the derivatives of the property with respect to the features"""
import numpy as np
from scipy import interpolate


def moving_avg_deriv(x, y, window_length=50):
    d = np.zeros(len(x) - window_length)
    for ind in range(len(x) - window_length):
        window = range(ind, ind + window_length)
        xy = np.sum((x[window] - np.mean(x[window])) * (y[window] - np.mean(y[window])))
        xx = np.sum((x[window] - np.mean(x[window])) * (x[window] - np.mean(x[window])))
        d[ind] = xy / xx
    return x[window_length:-window_length], d[:-window_length]


def get_deriv(val, prop, order=1):
    """Calculate the derivative of a property with respect to a given feature

    Args:
        val (np.ndarray(float)): The feature value array
        prop (np.ndarray(float)): The property vector

    Returns:
        val_avg:

    """
    test = np.dot(val, prop)
    inds = val.argsort()

    if not np.isfinite(test) or test == 0.0:
        raise ValueError("Invalid feature")

    if order > 3:
        raise ValueError("Requested order is too high")

    order_poly = min(10, max(75, len(val) // 20))
    x = val[inds].copy()
    y = prop[inds].copy()
    if order == 0:
        try:
            return np.polynomial.Legendre.fit(x, y, order_poly).linspace(1001)
        except np.linalg.LinAlgError:
            raise ValueError("Unable to perform initial polynomial fitting")

    for ii in range(order):
        try:
            poly = np.polynomial.Legendre.fit(x, y, order_poly)
        except np.linalg.LinAlgError:
            raise ValueError("Unable to perform initial polynomial fitting")
        x, y = moving_avg_deriv(*poly.linspace(1001))

    return x, y
