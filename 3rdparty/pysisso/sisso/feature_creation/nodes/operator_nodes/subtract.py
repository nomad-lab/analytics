"""File to describe SubNode"""

import sisso.feature_creation.nodes.functions as fxn
import sympy
from sisso.feature_creation.nodes.operator_node import OperatorNode


class SubNode(OperatorNode):

    """Node to add subtraction operators to the features
    """

    def __init__(self, feat_1, feat_2):
        """Node to add subtraction operators to the features

        Args:
            feat_1 (Node): Feature on the left of the subtraction
            feat_2 (Node): Feature on the right of the subtraction

        Raises:
            ValueError: If the unit for feat_1 and feat_2 are not the same, the resulting feature would be a constant or go outside accepted range
        # """
        if feat_1.unit != feat_2.unit:
            raise ValueError("When subtracting both features must have the same units")

        if feat_1 == feat_2:
            raise ValueError("When subtracting both features must be different")

        func = lambda x: fxn.sub(x, self._n_els)
        func.name = "sub"
        func.default_params = {}
        super(SubNode, self).__init__(func, "-", [feat_1, feat_2])

    @property
    def expr(self):
        """The sympy.Expression for the resulting feature

        Returns:
            sympy.Expression: The algebraic representation of the new feature
        """
        new_expr = (self.feats[0].fxn_in_expr) - (self.feats[1].fxn_in_expr)
        return sympy.trigsimp(new_expr)

    # @property
    def get_unit(self):
        """The sympy.Expression for the unit of the resulting feature

        Returns:
            sympy.Expression: The resulting unit of the feature
        """
        return self.feats[0].unit

    def reset_pivot(self, pvt):
        """reset the divider for self._func"""
        self._func = lambda x: fxn.sub(x, pvt)
        self._func.name = "sub"
        self._func.default_params = {}
