import numpy as np
from scipy import interpolate
import sympy

from .abs_diff import AbsDiffNode
from .absolute_value import AbsNode
from .add import AddNode
from .cos import CosNode
from .cube import CubNode
from .cube_root import CbrtNode
from .divide import DivNode
from .exponential import ExpNode, NegExpNode
from .inverse import InvNode
from .log import LogNode
from .multiply import MultNode
from .sin import SinNode
from .sixth_power import SixthPowerNode
from .square import SqNode
from .square_root import SqrtNode
from .subtract import SubNode

const = sympy.sympify("const")

op_map = {
    "add": AddNode,
    "sub": SubNode,
    "abs_diff": AbsDiffNode,
    "mult": MultNode,
    "div": DivNode,
    "exp": ExpNode,
    "neg_exp": NegExpNode,
    "inv": InvNode,
    "sq": SqNode,
    "cb": CubNode,
    "sixth_power": SixthPowerNode,
    "sqrt": SqrtNode,
    "cbrt": CbrtNode,
    "log": LogNode,
    "abs": AbsNode,
    "sin": SinNode,
    "cos": CosNode,
}

binary_ops = [AddNode, SubNode, AbsDiffNode, MultNode, DivNode]
commutative_ops = [AddNode, SubNode, AbsDiffNode, MultNode]
param_ops = [
    DivNode,
    AbsDiffNode,
    ExpNode,
    NegExpNode,
    InvNode,
    SqrtNode,
    CbrtNode,
    LogNode,
    SinNode,
    CosNode,
]
