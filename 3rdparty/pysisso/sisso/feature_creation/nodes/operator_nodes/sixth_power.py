"""File to describe SixthPowerNode"""

import sisso.feature_creation.nodes.functions as fxn
import sympy
from sisso.feature_creation.nodes.operator_node import OperatorNode


class SixthPowerNode(OperatorNode):

    """Node to raise a feature to the sixth power
    """

    def __init__(self, feat):
        """Node to raise a feature to the sixth power

        Args:
            feat (Node): Feature to add raise a feature to the sixth power
        Raises:
            ValueError: If feature would leave accepted range
        """

        disallowed = ["cbrt", "sqrt"]
        if feat.tag in disallowed:
            raise ValueError("Invalid feature combination")
        super(SixthPowerNode, self).__init__(fxn.six_pwr, "**6", feat)

    @property
    def expr(self):
        """The sympy.Expression for the resulting feature

        Returns:
            sympy.Expression: The algebraic representation of the new feature
        """
        return sympy.expand(
            sympy.trigsimp(sympy.powdenest(self.feat.fxn_in_expr ** 6, force=True))
        )

    # @property
    def get_unit(self):
        """The sympy.Expression for the unit of the resulting feature

        Returns:
            sympy.Expression: The resulting unit of the feature
        """
        return self.feat.unit ** 6.0
