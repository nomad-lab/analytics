"""Definition of the OperatorNode class"""
import warnings

import numpy as np
import sympy
from scipy import optimize

from sisso.feature_creation.nodes.unit import Unit
from sisso.feature_creation.nodes.node import Node

const = sympy.sympify("const")


class OperatorNode(Node):

    """Node used add mathematical/algebraic operations on top of the nodes
    """

    def __init__(self, func, tag, feats=None):
        """Node used add mathematical/algebraic operations on top of the nodes

        Args:
            func (Function): Function to apply to the attached Nodes
            tag (str): String tag that is used to represent the function in expressions
        """
        super(OperatorNode, self).__init__(tag=tag)

        self._func = func

        self._fxn_in_value = None

        self.feats = feats
        self._n_els = self.feats[0]._n_els

        self._expr = None
        self._fxn_in_expr = None
        self._unit = None

        self._params = None

    @property
    def func(self):
        """Accessor to the function

        Returns:
            Function: Function to apply to the attached Nodes
        """
        return self._func

    @property
    def value(self):
        """Calculate the value of the node by applying _func to all attached Nodes

        Returns:
            None: The result of applying _func to attached Nodes (None since this is a virtual class with no attached feature nodes)
        """
        if self._value is not None:
            return self._value

        return self._func(
            np.hstack([f.fxn_in_value for f in self._feats]), **self.params
        )

    def set_value(self):
        self._value = self._func(
            np.hstack([f.fxn_in_value for f in self._feats]), **self.params
        )

    @property
    def fxn_in_value(self):
        """Calculate the value of the node by applying _func to all attached Nodes

        Returns:
            None: The result of applying _func to attached Nodes (None since this is a virtual class with no attached feature nodes)
        """
        if self._fxn_in_value is not None:
            return self._fxn_in_value

        return self._func(
            np.hstack([f.fxn_in_value for f in self._feats]), **self.fxn_in_params
        )

    def set_fxn_in_value(self):
        self._fxn_in_value = self._func(
            np.hstack([f.fxn_in_value for f in self._feats]), **self.fxn_in_params
        )

    @property
    def expr(self):
        """Property accessor to the algebraic representation of the function.

        Returns:
            sympy.Expression: Expression used to describe the function
        """
        if self._expr is None:
            self._expr = self.get_expr()

        return self._expr

    def get_expr(self):
        """Accessor to the algebraic representation of the function.

        Returns:
            sympy.Expression: Expression used to describe the function
        """
        exprs = [sympy.simplify(f.fxn_in_expr) for f in self.feats]
        return sympy.Function(self._tag)(*exprs)

    @expr.setter
    def expr(self, expr):
        self._expr = expr

    @property
    def fxn_in_expr(self):
        """Property accessor to the algebraic representation of the function.

        Returns:
            sympy.Expression: Expression used to describe the function
        """
        if self._fxn_in_expr is None:
            self._fxn_in_expr = self.get_fxn_in_expr()

        return self._fxn_in_expr

    def get_fxn_in_expr(self):
        """Accessor to the algebraic representation of the function.

        Returns:
            sympy.Expression: Expression used to describe the function
        """
        return self.expr

    @fxn_in_expr.setter
    def fxn_in_expr(self, expr):
        self._fxn_in_expr = expr

    @property
    def unit(self):
        """Accessor function to the unit

        Returns:
            sympy.Expression: the updated unit after the operation is applied
        """
        if self._unit is None:
            self._unit = self.get_unit()

        return self._unit

    def get_unit(self):
        """Calculate the new unit after applying the operation on the feat

        Returns:
            sympy.Expression: Expression describing the new unit
        """
        return self._feats[0].unit

    @unit.setter
    def unit(self, unit):
        self._unit = unit

    @property
    def feats(self):
        """Accessor to feat

        Returns:
            Node: The node that the operator is to be applied to
        """
        return self._feats

    @feats.setter
    def feats(self, node_list):
        """Setter for feat

        Args:
            node (Node): The feature the operation is to be applied to

        Raises:
            TypeError: If node is not a type of Node
        """
        if issubclass(type(node_list), Node):
            self._feats = [node_list]
            return

        for node in node_list:
            if not issubclass(type(node), Node):
                raise TypeError("Feature node must be of a type derived from Node")

        self._feats = node_list

    @property
    def feat(self):
        return self._feats[0]

    def copy(self):
        """Copies a node

        Returns:
            to_cp(node.Node): A new Node that is the copy of the current node
        """
        to_cp = Node(value=self.value.copy(), tag=self._tag)
        to_cp._expr = self.expr
        to_cp._fxn_in_expr = self.fxn_in_expr
        to_cp.unit = self.unit
        return to_cp

    def set_default_params(self):
        self._params = self._func.default_params.copy()

    @property
    def params(self):
        """Property accessor to the algebraic representation of the function.

        Returns:
            sympy.Expression: Expression used to describe the function
        """
        if self._params is None:
            self._params = self._func.default_params.copy()

        return self._params

    @params.setter
    def params(self, params):
        self._params = params

    @property
    def fxn_in_params(self):
        params = self.params.copy()
        params.pop("c", None)
        params.pop("b", None)

        return self.params

    def parameterize(self, prop, mat_inds="all", fix_c_0=False):
        """Parameterize the function

        Args:
            prop (np.ndarray): The property to fit
            mat_inds (np.ndarray(int) or "all"): list of materials to include in parameterization
        """
        if isinstance(mat_inds, str) and mat_inds == "all":
            mat_inds = range(len(prop))
        try:
            self.reset_pivot(len(mat_inds))
        except AttributeError:
            pass

        mat_inds = list(np.array(mat_inds)[self.feat.fxn_in_value[mat_inds].argsort()])

        with warnings.catch_warnings():
            warnings.simplefilter("error", optimize.OptimizeWarning)
            self.params = self.initial_params(prop.flatten(), mat_inds)
            if fix_c_0:
                self.params = self.fxn_in_params
            try:
                params, _ = optimize.curve_fit(
                    self.func,
                    np.hstack([f.fxn_in_value[mat_inds] for f in self._feats]),
                    prop[mat_inds].flatten(),
                    list(self._params.values()),
                    check_finite=False,
                    bounds=self.bounds,
                )
                for key, val in zip(self._params.keys(), params):
                    self._params[key] = val
            except (RuntimeError, optimize.OptimizeWarning) as e:
                pass

        if fix_c_0:
            self._params["c"] = 0.0

        try:
            self.reset_pivot(self._n_els)
        except AttributeError:
            pass
