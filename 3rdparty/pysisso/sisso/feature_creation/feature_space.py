"""Defines the FeatureSpace class

Attributes:
    comm (mpi4py.MPI.COMM_WORLD): MPI communicator
    mpi_size (int): Number of MPI ranks in comm
    my_rank (int): Rank of this process
"""
import inspect
import math
import sys
from itertools import chain, combinations, islice, product
from pathlib import Path

import numpy as np
import pandas as pd
from scipy import stats

from time import time

from sisso.feature_creation.nodes import operator_nodes as operator_nodes
from sisso.feature_creation.nodes.feature_node import FeatureNode
from sisso.utils.mpi_interface import allgather_object, get_mpi_start_end_from_list


class FeatureSpace:

    """Class to describe the feature space at an arbitrary order

    Attributes:
        allowed_ops (List of Node classes): Allowed operations in the feature space
        list_sel_inds (list of lists of ints): List of selected indicies for each sis call
        max_abs_feat_val (float): Maximum absolute value of the features
        max_phi (int): Maximum depth of the operator trees
        n_sis_select (int): Number of features to select on each SIS step
        phi (list of lists of nodes): The full feature space
        phi_0 (list of Nodes): The initial feature space
        prop (np.ndarray): The property to be learned
        row_index (list of str): keys for the index describing each material
        selected_inds (list of ints): The indicies selected by SIS in a single list
    """

    def __init__(
        self,
        phi_0,
        prop,
        allowed_ops="all",
        max_phi=1,
        n_sis_select=1,
        parameterize=True,
        max_abs_feat_val=1e27,
        row_index=None,
        fix_c_0=False,
        cross_corr_max=0.95,
        learn_type="correlation",
        class_width=1e-6,
    ):
        """Initializer

        Args:
            phi_0 (list of Nodes): The initial feature space
            prop (np.ndarray, optional): The property to be learned
            allowed_ops (List of Node classes, optional): Allowed operations in the feature space
            max_phi (int, optional): Maximum depth of the operator trees
            n_sis_select (int, optional): Number of features to select on each SIS step
            max_abs_feat_val (float, optional): Maximum absolute value of the features
            row_index (list of str, optional): keys for the index describing each material
        """
        self.row_index = row_index
        self.fix_c_0 = fix_c_0

        phi_0_not_nan = [not feat.isnan(max_abs_feat_val) for feat in phi_0]
        phi_0 = list(np.array(phi_0)[np.where(phi_0_not_nan)[0]])

        self.phi_0 = phi_0
        self.phi_param = []
        self.phi_non_param = phi_0.copy()

        self.phi_param_gen_end = [0]
        self.phi_non_param_gen_end = [len(phi_0)]

        self.prop = prop
        self.prop_centered = (prop - prop.mean()).flatten()

        self.n_mats = len(prop)

        self._parameterize = parameterize
        self.cross_corr_max = cross_corr_max

        self.learn_type = learn_type
        self.class_width = class_width

        if allowed_ops == "all":
            clsmembers = inspect.getmembers(
                sys.modules["sisso.feature_creation.nodes.operator_nodes"],
                inspect.isclass,
            )
            self.allowed_ops = [member[1] for member in clsmembers]
        elif isinstance(allowed_ops[0], str):
            self.allowed_ops = [operator_nodes.op_map[op] for op in allowed_ops]
        else:
            self.allowed_ops = allowed_ops

        self.max_phi = max_phi
        self.max_abs_feat_val = max_abs_feat_val

        self.selected_inds = []
        self.list_sel_inds = []
        self.n_sis_select = n_sis_select

        self.generate_feature_space()

    @classmethod
    def from_df(
        cls,
        df,
        prop_key=None,
        allowed_ops="all",
        cols="all",
        max_phi=1,
        n_sis_select=1,
        max_abs_feat_val=1e27,
        parameterize=True,
        fix_c_0=False,
        cross_corr_max=0.95,
        learn_type="correlation",
        class_width=1e-6,
    ):
        """Construct a FeatureSpace from a DataFrame

        Args:
            df (pandas.DataFrame): DataFrame describing the initial FeatureSpace (features as columns)
            prop_key (None, optional): Key used to pull the property column
            allowed_ops (List of Node classes, optional): Allowed operations in the feature space
            cols (list of str, optional): Columns to pull from the DataFrame to add as the initial features
            max_phi (int, optional): Maximum depth of the operator trees
            n_sis_select (int, optional): Number of features to select on each SIS step
            max_abs_feat_val (float, optional): Maximum value of the features
        """
        if isinstance(df, str) or isinstance(df, Path):
            df = pd.read_csv(df, index_col=0)

        if prop_key is not None:
            prop = df[[prop_key]].to_numpy()
        else:
            prop = None

        df = df.drop([prop_key], axis=1)

        row_index = df.index

        if cols != "all":
            for col in df.columns.tolist():
                if col not in cols:
                    df = df.drop([col], axis=1)
        return cls(
            get_phi_0_from_df(df),
            prop,
            allowed_ops,
            max_phi,
            n_sis_select,
            parameterize,
            max_abs_feat_val,
            row_index,
            fix_c_0,
            cross_corr_max,
            learn_type,
            class_width,
        )

    def set_value_upto_rung(self, rung=2):
        """Set values of features up a certain rung"""
        if rung > self.max_phi:
            rung = self.max_phi
        start_p, end_p = get_mpi_start_end_from_list(self.phi_param_gen_end[rung])
        start_np, end_np = self.phi_non_param_gen_end[0] + np.array(
            get_mpi_start_end_from_list(
                self.phi_non_param_gen_end[rung] - self.phi_non_param_gen_end[0]
            )
        )

        for ii in range(start_p, end_p):
            self.phi_param[ii].set_value()
            self.phi_param[ii].set_fxn_in_value()

        for ii in range(start_np, end_np):
            self.phi_non_param[ii].set_value()
            self.phi_non_param[ii].set_fxn_in_value()

        self.phi_param = list(
            chain.from_iterable(allgather_object(self.phi_param[start_p:end_p]))
        )

        self.phi_non_param[self.phi_non_param_gen_end[0] :] = list(
            chain.from_iterable(allgather_object(self.phi_non_param[start_np:end_np]))
        )

    def pramaeterize(self, mat_inds):
        """Parameterize all parameterizable nodes

        Args:
            mat_inds (list of ints): Material indexes to include in the parameterization
        """

        if not self._parameterize:
            self.set_value_upto_rung()
            return

        start, end = get_mpi_start_end_from_list(len(self.phi_param))

        for feat in self.phi_param[start:end]:
            try:
                feat.parameterize(self.prop, mat_inds, self.fix_c_0)
            except ValueError:
                feat.set_default_params()

        self.phi_param = list(
            chain.from_iterable(allgather_object(self.phi_param[start:end]))
        )
        self.set_value_upto_rung()

    def generate_feature_space(self):
        """Generate the feature space from self.allowed_ops and self.phi_0
        """
        # Separate allowed_ops into unary and binary operators
        un_ops = []
        bi_ops = []
        com_bi_ops = []
        un_param_ops = []
        com_bi_param_ops = []

        for op in self.allowed_ops:
            if op not in operator_nodes.binary_ops and op in operator_nodes.param_ops:
                un_param_ops.append(op)
            elif op not in operator_nodes.binary_ops:
                un_ops.append(op)
            elif op not in operator_nodes.commutative_ops:
                bi_ops.append(op)
            elif op in operator_nodes.param_ops:
                com_bi_param_ops.append(op)
            else:
                com_bi_ops.append(op)

        # Generate New feature
        phi = self.phi_0.copy()
        rung_start = [0]

        for dim_lev in range(1, self.max_phi + 1):
            # Get initial start/end for current mpi rank
            start, end = get_mpi_start_end_from_list(
                len(phi) - rung_start[-1], rung_start[-1]
            )
            # Keep parameterizable separate
            next_phi_param = []
            for op in un_param_ops:
                for feat in phi[start:end]:
                    try:
                        next_phi_param.append(op(feat))
                    except ValueError:
                        continue
            next_phi = []
            for op in un_ops:
                for feat in phi[start:end]:
                    try:
                        next_phi.append(op(feat))
                    except ValueError:
                        continue

            phi_n_mn_1_generator = combinations(phi[rung_start[-1] :], 2)
            try:
                f = math.factorial
                n = len(phi) - rung_start[-1]
                len_gen = int(round(f(n) / (f(2) * f(n - 2))))
            except ValueError:
                continue
            start, end = get_mpi_start_end_from_list(len_gen)

            for feat_combi in islice(phi_n_mn_1_generator, start, end):
                for op in com_bi_ops:
                    try:
                        next_phi.append(op(feat_combi[0], feat_combi[1]))
                    except ValueError:
                        pass

                for op in com_bi_param_ops:
                    try:
                        next_phi_param.append(op(feat_combi[0], feat_combi[1]))
                    except ValueError:
                        pass

                for op in bi_ops:
                    try:
                        next_phi_param.append(op(feat_combi[0], feat_combi[1]))
                    except ValueError:
                        pass
                    try:
                        next_phi_param.append(op(feat_combi[1], feat_combi[0]))
                    except ValueError:
                        pass

            if len(rung_start) > 1:
                phi_lower_generator = product(
                    phi[rung_start[-1] :], phi[0 : rung_start[-1]]
                )
                len_gen = (len(phi) - rung_start[-1]) * (
                    rung_start[-1] - rung_start[-2]
                )
                start, end = get_mpi_start_end_from_list(len_gen)
                for feat_combi in islice(phi_lower_generator, start, end):
                    for op in com_bi_ops:
                        try:
                            next_phi.append(op(feat_combi[0], feat_combi[1]))
                        except ValueError:
                            continue
                    for op in com_bi_param_ops:
                        try:
                            next_phi_param.append(op(feat_combi[0], feat_combi[1]))
                        except ValueError:
                            continue
                    for op in bi_ops:
                        try:
                            next_phi_param.append(op(feat_combi[0], feat_combi[1]))
                        except ValueError:
                            continue
                        try:
                            next_phi_param.append(op(feat_combi[1], feat_combi[0]))
                        except ValueError:
                            continue

            not_nan = [not feat.isnan(self.max_abs_feat_val) for feat in next_phi]
            next_phi = np.array(next_phi)[np.where(not_nan)[0]]
            next_phi = list(chain.from_iterable(allgather_object(next_phi)))

            not_nan = [not feat.isnan(self.max_abs_feat_val) for feat in next_phi_param]
            next_phi_param = np.array(next_phi_param)[np.where(not_nan)[0]]
            next_phi_param = list(chain.from_iterable(allgather_object(next_phi_param)))

            rung_start.append(len(phi))
            phi += next_phi + next_phi_param

            self.phi_non_param += next_phi
            self.phi_param += next_phi_param
            self.phi_param_gen_end.append(len(self.phi_param))
            self.phi_non_param_gen_end.append(len(self.phi_non_param))

        self.pramaeterize(list(range(len(self.prop))))

    def get_selected_df(self, row_index=None, selected_inds=None):
        """Get a DataFame of selected features from phi

        Args:
            row_index (list of keys, optional): How to label the materials
            selected_inds (list of int, optional): The indices of the features to add to the DataFrame

        Returns:
            TYPE: Description
        """
        if row_index is None:
            row_index = self.row_index

        selected_nodes = self.get_select_nodes(selected_inds)

        col_headers = [f"{node.expr} ({node.unit})" for node in selected_nodes]
        data = np.array([node.fxn_in_value for node in selected_nodes])

        if row_index is None:
            return pd.DataFrame(data=data.T, columns=col_headers)

        return pd.DataFrame(data=data.T, columns=col_headers, index=row_index)

    @property
    def df(self):
        """Return a DataFrame containing only nodes selected by the SIS steps

        Returns:
            DaraFrame: DataFrame containing the selected nodes
        """
        selected_nodes = self.sis_selected
        col_headers = [f"{node.expr} ({node.unit})" for node in selected_nodes]
        data = np.array([node.fxn_in_value for node in selected_nodes])

        if self.row_index is None:
            return pd.DataFrame(data=data.T, columns=col_headers)
        return pd.DataFrame(data=data.T, columns=col_headers, index=self.row_index)

    @property
    def all_df(self):
        """Return a DataFrame containing only nodes selected by the SIS steps

        Returns:
            DaraFrame: DataFrame containing the selected nodes
        """
        selected_nodes = self.phi
        col_headers = [f"{node.expr} ({node.unit})" for node in selected_nodes]
        data = np.array([node.fxn_in_value for node in selected_nodes])

        if self.row_index is None:
            return pd.DataFrame(data=data.T, columns=col_headers)
        return pd.DataFrame(data=data.T, columns=col_headers, index=self.row_index)

    def project_stand(self, prop, inds, mat_inds, start=0, end=-1):
        prop_centered = [(pp - pp.mean()) / pp.std() for pp in prop]
        feat_vals = np.array(
            [feat.standardized_value[mat_inds] for feat in self.phi[inds[start:end]]]
        )
        return np.max(np.abs(np.dot(prop_centered, feat_vals.T)), axis=0)

    def project_r(self, prop, inds, mat_inds, start=0, end=-1):
        feat_vals = np.array(
            [feat.value[mat_inds] for feat in self.phi[inds[start:end]]]
        )
        return np.max(
            np.abs(
                [
                    (np.dot(pp, ff) - len(pp) * pp.mean() * ff.mean())
                    / (len(pp) * pp.std() * ff.std())
                    for pp in prop
                    for ff in feat_vals
                ]
            ).reshape(len(prop), -1),
            axis=0,
        )

    def project_log(self, prop, inds, mat_inds, start=0, end=-1):
        feat_vals = np.array(
            [feat.value[mat_inds] for feat in self.phi[inds[start:end]]]
        )
        coef_ints = np.array(
            [stats.linregress(fv, pp[mat_inds])[:2] for pp in prop for fv in feat_vals]
        ).reshape((len(prop), len(feat_vals), -1))
        return np.max(
            np.abs(
                [
                    np.sum(np.log10(np.abs(ci[0] * fv + ci[1] - p)))
                    for pp, p in enumerate(prop)
                    for ci, fv in zip(coef_ints[pp], feat_vals)
                ]
            ).reshape((len(prop), -1)),
            axis=0,
        )

    def convex1d_overlap(
        self, set_1, set_2, min_dist, inds, mat_inds, start, end, width=1e-6
    ):
        feat_vals_1 = np.array(
            [feat.value[mat_inds[set_1]] for feat in self.phi[inds[start:end]]]
        )

        feat_vals_2 = np.array(
            [feat.value[mat_inds[set_2]] for feat in self.phi[inds[start:end]]]
        )
        min_1 = np.min(feat_vals_1, axis=1)
        max_1 = np.max(feat_vals_1, axis=1)

        min_2 = np.min(feat_vals_2, axis=1)
        max_2 = np.max(feat_vals_2, axis=1)

        length = np.min(np.column_stack((max_1, max_2)), axis=1) - np.max(
            np.column_stack((min_1, min_2)), axis=1
        )
        is_overlap = length >= 0.0
        inds_no_overlap = np.where(is_overlap == False)[0]
        inds_overlap = np.where(is_overlap)[0]

        if len(inds_no_overlap) > 0:
            min_dist[inds_no_overlap] = np.max(
                np.column_stack((min_dist[inds_no_overlap], length[inds_no_overlap])),
                axis=1,
            )

        if len(inds_overlap) > 0:
            min_length = np.min(
                np.column_stack(
                    (
                        np.max(feat_vals_1, axis=1) - np.min(feat_vals_1, axis=1),
                        np.max(feat_vals_2, axis=1) - np.min(feat_vals_2, axis=1),
                    )
                ),
                axis=1,
            )
            overlap_length = np.zeros(len(min_length))
            overlap_length[inds_overlap] = (
                length[inds_overlap] / min_length[inds_overlap]
            )
            overlap_length[np.where(min_length == 0)[0]] = 1.0

        number = np.array(
            [
                len(np.where((f >= min_1[ff] - width) & (f <= max_1[ff] + width))[0])
                for ff, f in enumerate(feat_vals_2)
            ]
        )
        number += np.array(
            [
                len(np.where((f >= min_2[ff] - width) & (f <= max_2[ff] + width))[0])
                for ff, f in enumerate(feat_vals_1)
            ]
        )

        return number, overlap_length, min_dist

    def project_classification(self, prop, inds, mat_inds, start=0, end=-1):
        """Calculate the projection scores for classification problems

        Args:
            prop (np.ndarray): vector to project the features onto
            inds (np.ndarray): list of indicies to calculate the scores
            mat_inds (np.ndarray): list of indicies for which materials to use
            start(int, optional): index to begin projection calculation
            end(int, optional): index to end projection calculation

        Returns:
            scores (np.ndarray): scores used for SiS
        """
        prop_sorted = [np.sort(pp[mat_inds]) for pp in prop]
        prop_inds = [np.argsort(pp[mat_inds]) for pp in prop]

        overlap_length = np.zeros((len(inds[start:end]), len(prop)))
        overlap_n = np.zeros((len(inds[start:end]), len(prop)))
        min_dist = np.ones((len(inds[start:end]), len(prop)))
        min_dist *= -1.0 * self.max_abs_feat_val

        pp = -1
        for p_sort, p_inds in zip(prop_sorted, prop_inds):
            pp += 1
            test, start_group = np.unique(p_sort, return_index=True)
            for s1, start_1 in enumerate(start_group[:-1]):
                end_1 = start_group[s1 + 1]
                if p_sort[start_1] == 0:
                    continue
                for s2, start_2 in enumerate(start_group[s1 + 1 :]):
                    if s2 != len(start_group[s1 + 1 :]) - 1:
                        end_2 = start_group[s1 + 1 :][s2 + 1]
                    else:
                        end_2 = len(p_inds)
                    if p_sort[start_2] == 0:
                        continue
                    N, S, min_dist[:, pp] = self.convex1d_overlap(
                        p_inds[start_1:end_1],
                        p_inds[start_2:end_2],
                        min_dist[:, pp],
                        inds,
                        mat_inds,
                        start,
                        end,
                        width=self.class_width,
                    )
                    overlap_n[:, pp] += N
                    overlap_length[:, pp] += S

        is_overlap = np.abs(overlap_length) != 0.0
        inds_overlap = np.where(is_overlap)
        inds_no_overlap = np.where(is_overlap == False)
        min_dist[inds_overlap] = 0.0

        score = len(inds[start:end]) - overlap_n
        score *= 100.0 * max(
            1.0 / (1.0 + np.min(overlap_length)), np.max(np.abs(min_dist))
        )

        score[inds_overlap[0], inds_overlap[1]] += 1.0 / (
            overlap_length[inds_overlap[0], inds_overlap[1]] + 1.0
        )
        score[inds_no_overlap[0], inds_no_overlap[1]] -= min_dist[
            inds_no_overlap[0], inds_no_overlap[1]
        ]

        score = np.max(score, axis=1)
        sort_inds = np.argsort(score)[::-1]

        return score

    def sis(self, prop=None, mat_inds="all"):

        """Perform a round of SIS on the dataset

        Args:
            prop (np.ndarray, optional): vector to project the features onto
            mat_inds (list): list of materials to include

        Returns:
            float: The best feature's projection value

        Raises:
            ValueError: If prop and self.prop are None
        """
        if mat_inds == "all":
            mat_inds = range(len(self.prop))

        mat_inds = np.array(mat_inds)

        if prop is None:
            prop = [self.prop.flatten()[mat_inds]]

        inds = np.delete(
            np.arange(len(self.phi), dtype=int), np.array(self.selected_inds, dtype=int)
        )

        start, end = get_mpi_start_end_from_list(len(inds))
        if prop is None:
            prop = self.prop
        t0 = time()
        if self.learn_type == "log":
            projection_scores = self.project_log(prop, inds, mat_inds, start, end)
        elif self.learn_type == "correlation":
            projection_scores = self.project_r(prop, inds, mat_inds, start, end)
        elif self.learn_type == "classification":
            projection_scores = self.project_classification(
                prop, inds, mat_inds, start, end
            )

        # print(f"projection: {time()-t0}")
        projection_scores[np.where(np.isnan(projection_scores))[0]] = 0.0
        projection_scores = np.array(
            list(chain.from_iterable(allgather_object(projection_scores)))
        )
        indexes = projection_scores.argsort()[::-1]
        indices_sorted = inds[indexes]

        sel_inds = []

        D = np.zeros((self.n_sis_select, len(mat_inds)))
        if len(self.selected_inds) > 0:
            D = np.vstack(
                (
                    np.array(
                        [self.phi[ii].value[mat_inds] for ii in self.selected_inds]
                    ),
                    D,
                )
            )
            ii = 0
            dd = len(self.selected_inds)
        else:
            sel_inds.append(indices_sorted[0])
            D[0, :] = self.phi[indices_sorted[0]].value[mat_inds]
            ii = 1
            dd = 1

        # print(f"generate D: {time() - t0}")
        while len(sel_inds) < self.n_sis_select:
            stand_val = self.phi[indices_sorted[ii]].value[mat_inds]
            check_vals = np.array(
                [
                    np.abs(stats.linregress(stand_val, D[ff, :])[2])
                    for ff in range(D.shape[0])
                ]
            )
            if np.max(check_vals) < self.cross_corr_max and stand_val.std() > 1e-12:
                sel_inds.append(indices_sorted[ii])
                D[dd, :] = stand_val
                dd += 1
            ii += 1
        # print(f"populate_list: {time() - t0}")

        self.selected_inds += list(sel_inds)
        self.list_sel_inds.append(list(sel_inds))

    def get_select_nodes(self, selected_inds=None):
        """Get a subset of the nodes defined by selected_inds

        Args:
            selected_inds (list of ints, optional): indices used to get the selected nodes

        Returns:
            list of Nodes: The selected nodes
        """
        if selected_inds is None:
            selected_inds = self.selected_inds

        return self.phi[selected_inds]

    def clear_selection(self):
        """Clear all selected features (used for resets)"""
        self.selected_inds = []
        self.list_sel_inds = []

    @property
    def sis_selected(self):
        """Get the list of Nodes selected for by SIS

        Returns:
            list of Nodes: The features selected by SIS
        """
        return self.phi[self.selected_inds]

    @property
    def phi(self):
        """Get the complete feature space"""
        return np.array(self.phi_non_param + self.phi_param)

    def get_phi(self, rung=-1):
        """Get the complete feature space"""
        return np.array(
            self.phi_non_param[: self.phi_non_param_gen_end[rung]]
            + self.phi_param[: self.phi_param_gen_end[rung]]
        )


def get_phi_0_from_df(df):
    """Summary

    Args:
        df (pandas.DataFrame): The DataFrame to get phi_0 from

    Returns:
        list of FeatureNodes: The FeatureNodes of the columns in df
    """
    columns = df.columns.tolist()
    tags = list([col.split("(")[0] for col in columns])
    units = list([col.split("(")[1].split(")")[0] for col in columns])
    values = df.to_numpy().T
    phi_0 = []
    for tag, unit, value in zip(tags, units, values):
        if unit.lower() == "unitless" or unit.lower() == "":
            unit = None
        else:
            unit = unit.replace(" ", "")
        phi_0.append(FeatureNode(value, tag.replace(" ", ""), unit))
    return phi_0
