"""Class definition for a model"""
import numpy as np


class Model(object):
    """Store the selected models

    Attributes:
        feats (list of node.Nodes): Feature Nodes selected by SISSO
        coefs (list of floats): Coefficients to scale feat values
        intercept (float): intercept of the model
        rmse (float): RMSE of the model
    """

    def __init__(self, feats, prop, mat_inds, fix_c_0=False):
        """Constructor

        Args:
            feats (list of node.Nodes): Feature Nodes selected by SISSO
            coefs (list of floats): Coefficients to scale feat values
            intercept (float): intercept of the model
            rmse (float): RMSE of the model
        """
        self.feats = feats
        self.mat_inds = mat_inds
        D = np.array([node.value[mat_inds] for node in self.feats]).T

        if not fix_c_0:
            D = np.column_stack((D, np.ones(D.shape[0])))
        else:
            D = D.reshape(-1, len(self.coefs))

        coefs, square_error = np.linalg.lstsq(D, prop, rcond=-1)[:2]
        if fix_c_0:
            self.coefs = coefs
            self.intercept = 0
        else:
            self.coefs = coefs[:-1]
            self.intercept = coefs[-1]

        self.rmse = np.sqrt(square_error[0] / prop.size)
        self.fix_c_0 = fix_c_0

    @property
    def D(self):
        return np.array([node.value[self.mat_inds] for node in self.feats]).T

    @property
    def features(self):
        return [feat.expr for feat in self.feats]

    def predict(self, mat_inds=None):
        """Predict property value for a given set of materials

        Args:
            mat_inds (list of ints): indexes of materials to make a prediction on

        Returns:
            np.ndarray(float): The predicted property values
        """

        if mat_inds is None:
            mat_inds = range(len(self.feats[0].value))
        D = np.array([node.value[mat_inds] for node in self.feats]).T
        if not self.fix_c_0:
            D = np.column_stack((D, np.ones(D.shape[0])))
            return np.dot(D, np.append(self.coefs, self.intercept))
        else:
            D = D.reshape(-1, len(self.coefs))
            return np.dot(D, self.coefs)

    def __repr__(self):
        """Get the string representation of a model

        Args:
            features (list of str): str representation of the features
            i_iter (int): dimension to print

        Returns:
            str: The string representation of the model
        """
        import sympy

        expr = sympy.Float(self.intercept)

        for feat, coef in zip(self.feats, self.coefs.copy()):
            expr += sympy.simplify(coef * feat.expr)

        return sympy.pretty(expr, use_unicode=True)

    def copy(self):
        """Make a copy of the Model"""
        feats = [feat.copy() for feat in self.feats]
        return Model(feats, self.coefs.copy(), self.intercept, self.rmse)
