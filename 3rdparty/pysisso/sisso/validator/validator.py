"""Cross Validation Class"""

import numpy as np
import math
from itertools import combinations, islice


class Validator(object):
    """Base validator class

    Attributes:
        sisso_reg (SISSO_regressor.SISSO_Regressor): The SISSO Regression trained on all data
        feature_set (feature_space.FeatureSpace): The calculated feature space for the calculations
        num_mats (int): number of materials in the full data set
        shuffled_mat_inds (list of int): The indexes for the materials shuffled
        train_ind_lists (list of lists of ints): List of validation training sets
        test_ind_lists (list of lists of ints): List of validation testing sets
        models_list (list of SISSO_regressor.Model): All models trained from train_ind_list sets
        prediction_rmse (list of floats): list of the Prediction RMSE of each model
        prediction_max_ae (list of floats): list of prediction max absolute error for each model
    """

    def __init__(self, sisso):
        """Constructor

        Args:
            sisso(SISSO_regressor.SISSO_Regressor): The SISSO Regression trained on all data
        """
        self.sisso_reg = sisso
        self.feature_set = sisso.feature_set

        self.num_mats = len(self.feature_set.phi_0[0].value)
        self.shuffled_mat_inds = np.arange(self.num_mats, dtype=np.int64)
        np.random.shuffle(self.shuffled_mat_inds)
        self.train_ind_lists = []
        self.test_ind_lists = []

        self.models_list = []
        self.prediction_rmse = []
        self.prediction_max_ae = []

    def populate_mat_ind_lsts(self):
        """Dummy class to populate training/testing sets

        Not defined since no strategy passed
        """
        raise NotImplementedError(
            "populate_mat_ind_lsts is not defined for a generic validator"
        )

    def validate(self):
        """Validate the models

        Use the training/testing sets to validate a model
        """
        for train_inds, test_inds in zip(self.train_ind_lists, self.test_ind_lists):
            self.sisso_reg.reset(train_inds)
            self.sisso_reg.fit()

            models = self.sisso_reg.models.copy()
            self.models_list.append(models)

            prediction = [model[0].predict(test_inds) for model in models]
            predict_err = [
                (pp - self.feature_set.prop[test_inds].flatten()) for pp in prediction
            ]

            self.prediction_rmse.append(
                [np.sqrt(np.sum(pp ** 2.0) / len(pp)) for pp in predict_err]
            )
            self.prediction_max_ae.append([np.max(np.abs(pp)) for pp in predict_err])

        self.prediction_rmse = np.array(self.prediction_rmse)
        self.prediction_max_ae = np.array(self.prediction_max_ae)

    def summarize_error(self):
        """Get the summary of the error

        Returns:
            float: mean of the RMSE for each model
            float: mean of the max absolute error for each model
        """

        return (
            np.mean(self.prediction_rmse, axis=0),
            np.mean(self.prediction_max_ae, axis=0),
        )


class LeaveOutValidator(Validator):
    """Leave p validator class

    Attributes:
        max_iter (int): Maximum possible iteration value
        num_iter (int): Number of iterations of leave-p out validation to perform
        num_out (int): Number of materials to leave out for each validation step
    """

    def __init__(self, sisso, num_iter=None, num_out=None, frac=None):
        """Constructor

        Args:
            sisso(SISSO_regressor.SISSO_Regressor): The SISSO Regression trained on all data
            num_iter (int): Number of iterations of leave-p out validation to perform
            num_out (int): Number of materials to leave out for each validation step
            frac (float): Fraction of materials to leave out
        """
        if (num_iter is None) and ((frac is None) == (num_out is None)):
            raise ValueError(
                "leave out validation needs to have either frac or num_out defined."
            )

        super(LeaveOutValidator, self).__init__(sisso)

        if num_out is None:
            self.num_out = int(round(frac * self.num_mats))
        else:
            self.num_out = num_out

        if num_iter is None:
            num_iter = int(100)

        f = math.factorial
        self.max_iter = int(
            round(
                f(self.num_mats) / (f(self.num_out) * f(self.num_mats - self.num_out))
            )
        )
        self.num_iter = min(num_iter, self.max_iter)
        self.populate_mat_ind_lsts()

    def populate_mat_ind_lsts(self):
        """Set up Training/Testing sets for leave-p out cross validation"""
        self.train_ind_lists = []
        self.test_ind_lists = []

        if self.max_iter < 1e4:
            ind_comb = combinations(
                np.arange(len(self.shuffled_mat_inds), dtype=np.int64), self.num_out
            )
            inds = np.sort(
                np.random.choice(np.arange(self.max_iter), self.num_iter, False)
            )
            inds[1:] -= inds[0:-1] + 1
            for ind in inds:
                leave_out = np.array(list(islice(ind_comb, ind, ind + 1))[0])
                self.test_ind_lists.append(list(self.shuffled_mat_inds[leave_out]))
                self.train_ind_lists.append(
                    list(np.delete(self.shuffled_mat_inds, leave_out))
                )
        else:
            leave_out = np.sort(
                np.random.choice(self.num_mats, self.num_out, replace=False)
            )
            test_inds = self.shuffled_mat_inds[leave_out]
            self.test_ind_lists.append(test_inds)
            self.train_ind_lists.append(
                list(np.delete(self.shuffled_mat_inds, leave_out))
            )
            while len(self.test_ind_lists) < self.num_iter:
                leave_out = np.sort(
                    np.random.choice(self.num_mats, self.num_out, replace=False)
                )
                test_inds = self.shuffled_mat_inds[leave_out]
                if (
                    np.min(
                        np.sum(
                            np.abs(np.array(self.test_ind_lists) - test_inds), axis=1
                        )
                    )
                    > 1e-5
                ):
                    self.test_ind_lists.append(list(self.shuffled_mat_inds[leave_out]))
                    self.train_ind_lists.append(
                        list(np.delete(self.shuffled_mat_inds, leave_out))
                    )


class KFoldValidator(Validator):
    """K-Fold validator class

    Attributes:
        k_fold (int): Number of divisions to make
    """

    def __init__(self, sisso, k_fold=None):
        """Constructor

        Args:
            sisso(SISSO_regressor.SISSO_Regressor): The SISSO Regression trained on all data
            k_fold (int): Number of divisions to make
        """
        if k_fold is None:
            raise ValueError("k-fold validation needs to have n_fold defined")
        if k_fold <= 1:
            raise ValueError("k for k_fold must be greater than 1")

        super(KFoldValidator, self).__init__(sisso)
        self.k_fold = k_fold
        self.populate_mat_ind_lsts()

    def populate_mat_ind_lsts(self):
        """Set up Training/Testing sets for k-fold out cross validation"""
        self.train_ind_lists = []
        self.test_ind_lists = []

        for kk in range(self.k_fold):
            test_list = self.shuffled_mat_inds[kk :: self.k_fold]
            self.test_ind_lists.append(list(test_list))
            self.train_ind_lists.append(
                list(np.delete(self.shuffled_mat_inds, test_list))
            )
