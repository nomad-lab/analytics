import setuptools
from numpy.distutils.core import setup, Extension

setup(
    name="SISSO",
    version="0.0.1",
    description="Python implimentation of SISSO",
    author="Thomas Purcell, Emre Ahmeticik",
    author_email="purcell@fhi-berlin.mpg.de",
    license="BSD",
    install_requires=['numpy', 'sympy', 'pandas'],
    packages=setuptools.find_packages(),
    entry_points={
        "console_scripts": [
            "sisso = sisso.main:main",
        ]
    },
    zip_safe=False,
)

