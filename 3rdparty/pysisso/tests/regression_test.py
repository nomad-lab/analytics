
import numpy as np

from sisso.utils.mpi_interface import my_rank
from sisso.feature_creation.node.allowed_operator_nodes import op_map
from sisso.feature_creation.feature_space import FeatureSpace
from sisso.descriptor_identifcation.SISSO_regressor import SISSO_Regressor, print_models
from sisso.validator.validator import LeaveOutValidator, KFoldValidator

import warnings
warnings.filterwarnings("ignore")

cols = [
    "Z1 (C)",
    "Z2 (C)",
    "th (Unitless)",
]

# allowed_ops = [
#     op_map["sqrt"],
#     # op_map["sq"],
# ]

allowed_ops = list(op_map.values())

phi = FeatureSpace.from_df(
    "test.csv",
    "prop_3",
    allowed_ops,
    cols,
    2,
    1,
)

sisso = SISSO_Regressor(
    phi,
    1,
    True,
)
sisso.fit()
models = sisso.models
if my_rank == 0:
    print("Training Results")
    print_models(models)
