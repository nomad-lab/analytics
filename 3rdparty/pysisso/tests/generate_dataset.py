import numpy as np
import pandas as pd
from scipy import interpolate
from matplotlib import pyplot as plt
from scipy.signal import savgol_filter
# import statsmodels.api as sm

rng = np.random.RandomState(13)
from statsmodels.nonparametric.kernel_regression import KernelReg

anions = np.array([6, 14, 32, 7, 15, 33, 51, 8, 16, 34, 52, 9, 17, 35, 53])
cations = np.delete(np.arange(54, dtype=np.int64), anions)

n_pts = 100

Z1 = rng.choice(cations, n_pts)
Z2 = rng.choice(anions, n_pts)

th = rng.rand(n_pts) * np.pi * 2.0 - np.pi

prop_1 = np.exp(-th**2.0 / 2.0) / 2.0 + 2.0

prop_2 = prop_1 + (rng.normal(size=n_pts, scale=1.0))

prop_3 = -853.0*np.sqrt(25.0*np.pi - 5.458752*th) + 100.0
prop_3 = 1.0 / (0.56*th**2.0 + 4.548420) - 15800 + (rng.normal(size=n_pts, scale=0.025))
prop_3 = -np.exp(-th)
# prop_3 = Z1 * Z2 / np.exp(th / 2.0) # * (197.0 / (137.0 * 4.0) )

cols = ["prop_1", "prop_2", "prop_3", "Z1 (C)", "Z2 (C)", "th (Unitless)"]
data = np.vstack((prop_1, prop_2, prop_3, Z1, Z2, th))
pd.DataFrame(data.T, columns=cols).to_csv("test.csv", index_label="sample")

inds = th.argsort()
x = th[inds]
y = prop_2[inds]
# plt.plot(x, y, 'b.')
plt.plot(x, prop_3[inds], 'b.')

# wind = n_pts // 5
# wind += (wind + 1) % 2
# for i in range(1):
#     y = savgol_filter(y, wind, 5)
# plt.plot(x, y)

spl = interpolate.UnivariateSpline(th[inds], prop_3[inds], s=1000.0, k=5)
plt.plot(x, spl(x))
# kr = KernelReg(prop_2[inds], th[inds], 'c')
# x_pred = np.linspace(x[0], x[-1], 100)
# y_pred, y_std = kr.fit(x_pred)

# plt.plot(x_pred, y_pred)
plt.show()
